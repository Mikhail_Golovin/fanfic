package com.golovin.server.Services;

import com.golovin.server.DAO.*;
import com.golovin.server.Services.*;
import com.golovin.server.models.Books;
import com.golovin.server.models.LastBook;
import com.golovin.server.models.Library;
import com.golovin.server.models.User;
import com.golovin.server.utils.MyCommon;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

@ExtendWith(MockitoExtension.class)
public class BaseTest {
    @Mock
    protected UserDao userDao;
    @Mock
    protected ChaptersDao chaptersDao;
    @Mock
    protected CompletenessDao completenessDao;
    @Mock
    protected CoverDao coverDao;
    @Mock
    protected GenresDao genresDao;
    @Mock
    protected LastBookDao lastBookDao;
    @Mock
    protected LibraryDao libraryDao;
    @Mock
    protected StatusDao statusDao;
    @Mock
    protected BookDao bookDao;

    @InjectMocks
    protected BookService bookService;
    @Mock
    protected ChaptersService chaptersService;
    @InjectMocks
    protected CompletenessService completenessService;
    @InjectMocks
    protected CoverService coverService;
    @InjectMocks
    protected GenresService genresService;
    @InjectMocks
    protected LastBookService lastBookService;
    @InjectMocks
    protected LibraryService libraryService;
    @InjectMocks
    protected StatusService statusService;
    @Spy
    protected UserService userService;

    @InjectMocks
    protected MyCommon myCommon;

    @BeforeEach
    void beforeEach() {

        Whitebox.setInternalState(BookService.class,"bookDao", bookDao);
        Whitebox.setInternalState(ChaptersService.class,"chaptersDao", chaptersDao);
        Whitebox.setInternalState(CompletenessService.class,"completenessDao", completenessDao);
        Whitebox.setInternalState(CoverService.class,"coverDao", coverDao);
        Whitebox.setInternalState(GenresService.class,"genresDao", genresDao);
        Whitebox.setInternalState(LastBookService.class,"lastBookDao", lastBookDao);
        Whitebox.setInternalState(LibraryService.class,"libraryDao", libraryDao);
        Whitebox.setInternalState(StatusService.class,"statusDao", statusDao);
        Whitebox.setInternalState(UserService.class,"userDao", userDao);


        Whitebox.setInternalState(MyCommon.class, "userService",userService);
        Whitebox.setInternalState(MyCommon.class,"chapterService",chaptersService);
        Whitebox.setInternalState(MyCommon.class,"bookService",bookService);
        Whitebox.setInternalState(MyCommon.class,"completenessService",completenessService);
        Whitebox.setInternalState(MyCommon.class,"coverService",coverService);
        Whitebox.setInternalState(MyCommon.class,"genresService",genresService);
        Whitebox.setInternalState(MyCommon.class,"lastBookService",lastBookService);
        Whitebox.setInternalState(MyCommon.class,"libraryService",libraryService);
        Whitebox.setInternalState(MyCommon.class,"statusService",statusService);
    }
}
