package com.golovin.server.Services;
import com.golovin.server.DAO.CompletenessDao;
import com.golovin.server.Services.CompletenessService;
import com.golovin.server.models.Completeness;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CompletenessServiceTest {

    @Mock
    private CompletenessDao completenessDao;

    @Mock
    private Session session;

    @InjectMocks
    private CompletenessService completenessService;




    @Test
    void getById() {
        Whitebox.setInternalState(CompletenessService.class,"completenessDao", completenessDao);
        Whitebox.setInternalState(MyCommon.class, "completenessService",completenessService);
        completenessService = new CompletenessService();


        Completeness completeness = new Completeness();
        completeness.setId(1);
        completeness.setCompleteness("Завершено");

        when(completenessDao.openCurrentSession()).thenReturn(session);

        when(completenessDao.findById(session, 1)).thenReturn(completeness);
        Completeness completenessTest = completenessService.getById(1);
        assertEquals(completenessTest.getCompleteness(),completeness.getCompleteness());
        assertEquals(completenessTest.getId(), completeness.getId());

        when(completenessDao.findById(session, 3)).thenReturn(null);
        completenessTest = completenessService.getById(3);
        assertNull(completenessTest);
    }
}