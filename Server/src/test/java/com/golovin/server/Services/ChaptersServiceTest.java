package com.golovin.server.Services;

import com.golovin.server.DAO.ChaptersDao;
import com.golovin.server.Services.BookService;
import com.golovin.server.Services.ChaptersService;
import com.golovin.server.Services.UserService;
import com.golovin.server.classes.Chapter;
import com.golovin.server.classes.ChapterClass;
import com.golovin.server.models.Books;
import com.golovin.server.models.Chapters;
import com.golovin.server.models.User;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ChaptersServiceTest {

    @InjectMocks
    private ChaptersService chaptersService;

    @Mock
    private ChaptersDao chaptersDao;

    @Mock
    private UserService userService;

    @Mock
    private Session session;

    @Mock
    private BookService bookService;

    @InjectMocks
    protected MyCommon myCommon;

    private Chapters chapters1;

    private Chapters chapters2;

    private Chapters chapters3;

    private List<Chapters> chaptersList = new ArrayList<>();

    private User user1;

    private User user2;

    private Books book1;

    private Books book2;

    @BeforeEach
    void beforeEach() {
        Whitebox.setInternalState(ChaptersService.class, "chaptersDao", chaptersDao);
        Whitebox.setInternalState(MyCommon.class, "chapterService", chaptersService);
        Whitebox.setInternalState(MyCommon.class, "userService", userService);
        Whitebox.setInternalState(MyCommon.class, "bookService", bookService);

        book1 = new Books();
        book1.setId(1);
        chapters1 = new Chapters("Name1", 1, "Text1", book1);
        chapters1.setId(1);
        book1.addChapter(chapters1);

        book2 = new Books();
        book2.setId(2);
        chapters2 = new Chapters("Name2", 1, "Text2", book2);
        chapters2.setId(2);
        chapters3 = new Chapters("Name3", 2, "Text3", book2);
        chapters3.setId(3);
        book2.addChapter(chapters2);
        book2.addChapter(chapters3);

        chaptersList.add(chapters1);
        chaptersList.add(chapters2);

        user1 = new User("Name1","Surname1","auth1");
        user1.setId(1);
        user1.addChapter(chapters1);
        //user1.addChapter(chapters3);

        user2 = new User("Name2","Surname2","auth2");
        user2.setId(2);
        //user2.addChapter(chapters3);
    }

    @Test
    void getById() {
        when(chaptersDao.openCurrentSession()).thenReturn(session);
        when(chaptersDao.findById(session, 1)).thenReturn(chapters1);
        Chapters chapter = chaptersService.getById(1);
        assertNotNull(chapter);
        assertEquals(chapter.getId(), 1);
        assertEquals(chapter.getNumber(), 1);
        assertEquals(chapter.getText(), "Text1");
        assertEquals(chapter.getBook().getId(), 1);
    }

    @Test
    void update() {
        when(chaptersDao.openCurrentSessionWithTransaction()).thenReturn(session);
        ArgumentCaptor<Chapters> valueCapture = ArgumentCaptor.forClass(Chapters.class);
        doNothing().when(chaptersDao).update(any(Session.class), valueCapture.capture());
        chaptersService.update(chapters1);
        assertEquals(chapters1.getName(), valueCapture.getValue().getName());
        assertEquals(chapters1.getText(), valueCapture.getValue().getText());
        assertEquals(chapters1.getId(), valueCapture.getValue().getId());

        chaptersService.update(chapters2);
        assertEquals(chapters2.getName(), valueCapture.getValue().getName());
        assertEquals(chapters2.getText(), valueCapture.getValue().getText());
        assertEquals(chapters2.getId(), valueCapture.getValue().getId());
    }

    @Test
    void findAll() {
        when(chaptersDao.openCurrentSession()).thenReturn(session);
        when(chaptersDao.findAll(session)).thenReturn(chaptersList);
        List<Chapters> chaptersList = chaptersService.findAll();
        chaptersList.sort(Comparator.comparingInt(Chapters::getId));
        assertNotNull(chaptersList);
        Chapters testChapter = chaptersList.get(0);

        assertEquals(chapters1.getName(), testChapter.getName());
        assertEquals(chapters1.getText(), testChapter.getText());
        assertEquals(chapters1.getId(), testChapter.getId());

    }

    @Test
    void findChapterOfBook() {
        when(chaptersDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(chaptersDao.findChapterOfBook(session, 1)).thenReturn(chapters1);
        ChapterClass chapter = chaptersService.findChapterOfBook(1);
        assertEquals(chapter.getChapterId(), chapters1.getId());
        assertEquals(chapter.getText(), chapters1.getText());
    }

    @Test
    void findLastChapter() {
        when(chaptersDao.openCurrentSession()).thenReturn(session);
        when(chaptersDao.findLastChapter(session, 2)).thenReturn(chapters3);
        Chapters chapter = chaptersService.findLastChapter(2);
        assertEquals(chapter.getId(), chapters3.getId());
        assertEquals(chapter.getName(), chapters3.getName());
        assertEquals(chapter.getNumber(), chapters3.getNumber());
        assertEquals(chapter.getBook().getId(), chapters3.getBook().getId());
    }

    @Test
    void findFirstChapter() {
        when(chaptersDao.openCurrentSession()).thenReturn(session);
        when(chaptersDao.findFirstChapter(session, 2)).thenReturn(chapters2);
        Chapters chapter = chaptersService.findFirstChapter(2);
        assertEquals(chapter.getId(), chapters2.getId());
        assertEquals(chapter.getName(), chapters2.getName());
        assertEquals(chapter.getNumber(), chapters2.getNumber());
        assertEquals(chapter.getBook().getId(), chapters2.getBook().getId());
    }

    @Test
    void addChapter() {
        Chapter chapter = new Chapter("TestName", "TestText");

        when(chaptersDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(chaptersDao.openCurrentSession()).thenReturn(session);
        when(bookService.getById(2)).thenReturn(book2);
        when(chaptersDao.findLastChapter(session, 2)).thenReturn(chapters3);
        doNothing().when(bookService).update(book2);


        Chapters chaptersTest = new Chapters(chapter.getName(), 3, chapter.getText(), book2);
        doNothing().when(chaptersDao).persist(session, chaptersTest);
        int idChapter = chaptersService.addChapter(2, chapter);

        assertEquals(chapter.getName(), chapter.getName());
        assertEquals(chapter.getText(), chapter.getText());
        assertEquals(0, idChapter);
    }

    @Test
    void changeTextInChapter() {
        when(chaptersDao.openCurrentSession()).thenReturn(session);
        when(chaptersDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(chaptersDao.findById(session, 2)).thenReturn(chapters2);
        doNothing().when(bookService).update(book2);
        doNothing().when(chaptersDao).update(session, chapters2);

        Chapter chapter = new Chapter("TestName", "TestText");

        boolean isChanged = chaptersService.changeTextInChapter(chapters2.getId(), chapter);
        assertTrue(isChanged);
        assertEquals(chapters2.getText(),chapter.getText());
        assertEquals(chapters2.getName(),chapter.getName());

    }

    @Test
    void deleteChapter() {
        when(chaptersDao.openCurrentSession()).thenReturn(session);
        when(chaptersDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(chaptersDao.findById(session, 3)).thenReturn(chapters3);
        doNothing().when(bookService).update(book2);

        assertEquals(2,book2.getChaptersList().size());
        boolean isDeleted = chaptersService.deleteChapter(3);
        assertTrue(isDeleted);
        assertEquals(1,book2.getChaptersList().size());
    }
}