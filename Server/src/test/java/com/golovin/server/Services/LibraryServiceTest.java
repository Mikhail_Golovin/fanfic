package com.golovin.server.Services;

import com.golovin.server.DAO.LibraryDao;
import com.golovin.server.Services.BookService;
import com.golovin.server.Services.LibraryService;
import com.golovin.server.Services.StatusService;
import com.golovin.server.Services.UserService;
import com.golovin.server.classes.ListBooksId;
import com.golovin.server.classes.SearchParamsClass;
import com.golovin.server.models.*;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LibraryServiceTest {


    @InjectMocks
    private LibraryService libraryService;

    @Mock
    private LibraryDao libraryDao;

    @Mock
    private Session session;

    @Mock
    private StatusService statusService;

    @Mock
    private UserService userService;

    @Mock
    private BookService bookService;

    @InjectMocks
    protected MyCommon myCommon;

    private Library library1;

    private Library library2;

    private Library library3;

    private Library library4;


    private final List<Library> libraryList1 = new ArrayList<>();
    private final List<Library> libraryList2 = new ArrayList<>();


    @BeforeEach
    void beforeEach() {
        Whitebox.setInternalState(LibraryService.class, "libraryDao", libraryDao);
        Whitebox.setInternalState(MyCommon.class, "libraryService", libraryService);
        Whitebox.setInternalState(MyCommon.class, "statusService", statusService);
        Whitebox.setInternalState(MyCommon.class, "bookService", bookService);
        Whitebox.setInternalState(MyCommon.class, "userService", userService);

        User user = new User();
        user.setId(1);
        Books books = new Books();
        books.setId(1);
        Status status = new Status();
        status.setId(1);
        library1 = new Library(new LibraryPK(books.getId(), status.getId(), user.getId()), books, status, user);

        User user2 = new User();
        user2.setId(2);
        Books books2 = new Books();
        books2.setId(2);
        Status status2 = new Status();
        status2.setId(1);
        library2 = new Library(new LibraryPK(books2.getId(), status2.getId(), user2.getId()), books2, status2, user2);

        libraryList1.add(library1);
        libraryList1.add(library2);


        User user3 = new User();
        user3.setId(3);
        Books books3 = new Books();
        books3.setId(3);
        Status status3 = new Status();
        status3.setId(1);
        library3 = new Library(new LibraryPK(books3.getId(), status3.getId(), user3.getId()), books3, status3, user3);

        User user4 = new User();
        user4.setId(3);
        Books books4 = new Books();
        books4.setId(4);
        Status status4 = new Status();
        status4.setId(2);
        library4 = new Library(new LibraryPK(books4.getId(), status4.getId(), user4.getId()), books4, status4, user4);

        libraryList2.add(library3);
        libraryList2.add(library4);

    }

    @Test
    void getBooksFromLibraryWithStatus() {
        when(libraryDao.openCurrentSession()).thenReturn(session);
        when(libraryDao.getBooksWithStatus(session, 1, 1)).thenReturn(libraryList1);
        List<Library> libraryListTest = libraryService.getBooksFromLibraryWithStatus(1, 1);
        assertNotNull(libraryListTest);
        assertEquals(1, libraryListTest.get(0).getPk().getBookId());
        assertEquals(2, libraryListTest.get(1).getBook().getId());
        assertEquals(1, libraryListTest.get(0).getUser().getId());
        assertEquals(2, libraryListTest.get(1).getPk().getUserId());
    }

    @Test
    void getAllBooksFromLibrary() {
        when(libraryDao.openCurrentSession()).thenReturn(session);
        when(libraryDao.getAllBooks(session, 3)).thenReturn(libraryList2);
        ListBooksId listBooksId = libraryService.getAllBooksFromLibrary(3);
        listBooksId.getBooksId().sort(Integer::compareTo);
        assertNotNull(listBooksId);
        assertEquals(3, listBooksId.getBooksId().get(0));
        assertEquals(4, listBooksId.getBooksId().get(1));
    }

    @Test
    void getUserBookStatus() {
        when(libraryDao.openCurrentSession()).thenReturn(session);
        when(libraryDao.getUserBookStatus(session, 3, 3)).thenReturn(library3);
        int status = libraryService.getUserBookStatus(3, 3);
        assertEquals(1, status);

        when(libraryDao.getUserBookStatus(session, 3, 5)).thenReturn(null);
        status = libraryService.getUserBookStatus(3, 5);
        assertEquals(5, status);
    }

    @Test
    void addBookInLibrary() {

        when(libraryDao.openCurrentSessionWithTransaction()).thenReturn(session);
        User testUser = library4.getUser();
        Books testBook = library4.getBook();
        Status testStatus = library4.getStatus();


        when(statusService.getById(testStatus.getId())).thenReturn(testStatus);
        when(userService.getById(testUser.getId())).thenReturn(testUser);
        when(bookService.getById(testBook.getId())).thenReturn(testBook);


        when(libraryDao.addBookInLibrary(session, testUser.getId(), testStatus.getId(), testBook.getId())).thenReturn(library4);
        int status = libraryService.addBookInLibrary(testUser.getId(), testStatus.getId(), testBook.getId());
        assertEquals(testStatus.getId(), status);

    }

    @Test
    void searchBooks() {
        when(libraryDao.openCurrentSession()).thenReturn(session);
        SearchParamsClass searchParams = new SearchParamsClass("а", 1, 1);
        when(libraryDao.searchBooks(session, searchParams.getText(), searchParams.getGenreId(),
                searchParams.getSortingType(), 3)).thenReturn(libraryList2);
        ListBooksId listBooksId = libraryService.searchBooks(searchParams, 3);
        assertEquals(listBooksId.getBooksId().size(),2);
        assertEquals(listBooksId.getBooksId().get(0),libraryList2.get(0).getBook().getId());
        assertEquals(listBooksId.getBooksId().get(1),libraryList2.get(1).getBook().getId());
    }
}