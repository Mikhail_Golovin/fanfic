package com.golovin.server.Services;

import com.golovin.server.DAO.UserDao;
import com.golovin.server.Services.ChaptersService;
import com.golovin.server.Services.UserService;
import com.golovin.server.classes.UserClass;
import com.golovin.server.models.Books;
import com.golovin.server.models.Chapters;
import com.golovin.server.models.User;
import com.golovin.server.models.User;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {


    @Mock
    protected ChaptersService chaptersService;
    @InjectMocks
    protected UserService userService;

    @Mock
    protected UserDao userDao;

    @Mock
    private Session session;

    @InjectMocks
    protected MyCommon myCommon;

    private User user1;

    private User user2;

    private final List<User> userList = new ArrayList<>();

    @BeforeEach
    void beforeEach(){
        Whitebox.setInternalState(UserService.class,"userDao", userDao);
        Whitebox.setInternalState(MyCommon.class, "userService",userService);
        Whitebox.setInternalState(MyCommon.class,"chapterService",chaptersService);

        user1 = new User("User1Name","User1Surname","User1Auth");
        user1.setId(1);

        user2= new User("User2Name","User2Surname","User3Auth");
        user2.setId(2);

        userList.add(user1);
        userList.add(user2);
    }

    @Test
    void getById() {
        when(userDao.openCurrentSession()).thenReturn(session);
        when(userDao.findById(session, 1)).thenReturn(user1);
        User userTest = userService.getById(1);
        assertEquals(user1.getName(),userTest.getName());
        assertEquals(user1.getSurname(),userTest.getSurname());
        assertEquals(user1.getGoogle_auth(),userTest.getGoogle_auth());

        when(userDao.findById(session, -2)).thenReturn(null);
        userTest = userService.getById(-2);
        assertNull(userTest);

    }

    @Test
    void persist() {
        when(userDao.openCurrentSessionWithTransaction()).thenReturn(session);
        ArgumentCaptor<User> valueCapture = ArgumentCaptor.forClass(User.class);
        doNothing().when(userDao).persist(any(Session.class), valueCapture.capture());
        userService.persist(user1);
        assertEquals(user1.getName(), valueCapture.getValue().getName());
        assertEquals(user1.getSurname(), valueCapture.getValue().getSurname());
        assertEquals(user1.getGoogle_auth(), valueCapture.getValue().getGoogle_auth());
    }

    @Test
    void update() {
        when(userDao.openCurrentSessionWithTransaction()).thenReturn(session);
        ArgumentCaptor<User> valueCapture = ArgumentCaptor.forClass(User.class);
        doNothing().when(userDao).update(any(Session.class), valueCapture.capture());
        userService.update(user1);
        assertEquals(user1.getName(), valueCapture.getValue().getName());
        assertEquals(user1.getSurname(), valueCapture.getValue().getSurname());
        assertEquals(user1.getGoogle_auth(), valueCapture.getValue().getGoogle_auth());

        userService.update(user2);
        assertEquals(user2.getName(), valueCapture.getValue().getName());
        assertEquals(user2.getSurname(), valueCapture.getValue().getSurname());
        assertEquals(user2.getGoogle_auth(), valueCapture.getValue().getGoogle_auth());
    }

    @Test
    void findAll() {
        when(userDao.openCurrentSession()).thenReturn(session);
        when(userDao.findAll(session)).thenReturn(userList);
        List<User> userList = userService.findAll();
        userList.sort(Comparator.comparingInt(User::getId));
        assertNotNull(userList);
        User testUser = userList.get(0);

        assertEquals(user1.getName(), testUser.getName());
        assertEquals(user1.getSurname(), testUser.getSurname());
        assertEquals(user1.getGoogle_auth(), testUser.getGoogle_auth());
    }

    @Test
    void getUser() {
        when(userDao.openCurrentSession()).thenReturn(session);
        when(userDao.findById(session, -1)).thenReturn(null);
        UserClass userClass = userService.getUser(-1);
        assertEquals(userClass.getName(), "Пожалуйста");
        assertEquals(userClass.getSurname(), "Авторизуйтесь");

        when(userDao.findById(session, 1)).thenReturn(user1);
        userClass = userService.getUser(1);
        assertEquals(userClass.getName(), "User1Name");
        assertEquals(userClass.getSurname(), "User1Surname");
    }

    @Test
    void findUserAuth() {
        when(userDao.openCurrentSession()).thenReturn(session);
        when(userDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(userDao.findUserAuth(session, user1)).thenReturn(user1.getId());
        int userId = userService.findUserAuth(user1.getName(), user1.getSurname(), user1.getGoogle_auth());
        assertEquals(userId, 1);

        when(userDao.findUserAuth(session, user2)).thenReturn(null);
        doNothing().when(userDao).persist(session, user2);
        userService.persist(user2);
        userId = userService.findUserAuth(user2.getName(), user2.getSurname(), user2.getGoogle_auth());
        assertEquals(userId, 0);

    }

    @Test
    void getLastChapter() {
        Books book1 = new Books();
        book1.setId(1);
        Chapters chapters1 = new Chapters("name1",1,"text1",book1);
        chapters1.setId(1);

        Books book2 = new Books();
        book1.setId(2);
        Chapters chapters2 = new Chapters("name2",2,"text2",book2);
        chapters2.setId(2);

        Books book3 = new Books();
        book3.setId(3);
        Chapters chapters3 = new Chapters("name3",3,"text3",book3);
        chapters3.setId(3);


        user1.addChapter(chapters1);
        user1.addChapter(chapters2);

        when(userDao.openCurrentSession()).thenReturn(session);

        when(userDao.findById(session, 1)).thenReturn(user1);

        when(chaptersService.findFirstChapter(3)).thenReturn(chapters3);

        int lastChapterId = userService.getLastChapter(-1, 3);
        assertEquals(lastChapterId, 3);

        when(chaptersService.findFirstChapter(1)).thenReturn(chapters1);
        lastChapterId = userService.getLastChapter(1, 1);
        assertEquals(lastChapterId, chapters1.getId());
        user1.removeChapter(chapters1);
        user1.removeChapter(chapters2);

    }

    @Test
    void changeLastChapter() {
        when(userDao.openCurrentSession()).thenReturn(session);
        when(userDao.findById(session, 1)).thenReturn(user1);
        Books book1 = new Books();
        book1.setId(1);
        Chapters chapters1 = new Chapters("name1",1,"text1",book1);
        chapters1.setId(1);
        user1.addChapter(chapters1);

        when(chaptersService.getById(1)).thenReturn(chapters1);
        userService.changeLastChapter(-1, 4);

        userService.changeLastChapter(1,1);
        int userLastChapter = userService.getLastChapter(1, 1);
        assertEquals(1, userLastChapter);
    }
}