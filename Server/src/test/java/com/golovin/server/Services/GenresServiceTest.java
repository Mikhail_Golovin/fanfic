package com.golovin.server.Services;

import com.golovin.server.DAO.GenresDao;
import com.golovin.server.Services.GenresService;
import com.golovin.server.models.Genres;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GenresServiceTest {

    @Mock
    private GenresDao genresDao;

    @Mock
    private Session session;

    @InjectMocks
    private GenresService genresService;

    private Genres genres1;

    private Genres genres2;

    private final List<Genres> genresList = new ArrayList<>();




    @BeforeEach
    void beforeEach(){
        Whitebox.setInternalState(MyCommon.class,"genresService", genresService);
        Whitebox.setInternalState(GenresService.class,"genresDao", genresDao);


        genres1 = new Genres();
        genres1.setGenre("Фантастика");
        genres1.setId(1);

        genres2 = new Genres();
        genres2.setGenre("Боевик");
        genres2.setId(4);

        genresList.add(genres1);
        genresList.add(genres2);

    }

    @Test
    void getById() {
        when(genresDao.openCurrentSession()).thenReturn(session);
        when(genresDao.findById(session,1)).thenReturn(genres1);

        Genres genresTest = genresService.getById(1);
        assertEquals(genres1.getGenre(), genresTest.getGenre());

        when(genresDao.findById(session, 4)).thenReturn(genres2);
        genresTest = genresService.getById(4);
        assertEquals(genres2.getGenre(), genresTest.getGenre());
        when(genresDao.findById(session, 11)).thenReturn(null);


        genresTest = genresService.getById(11);
        assertNull(genresTest);
    }

    @Test
    void findAll() {
        when(genresDao.openCurrentSession()).thenReturn(session);
        when(genresDao.findAll(session)).thenReturn(genresList);
        List<Genres> genres = genresService.findAll();
        assertNotNull(genres);
        assertEquals("Фантастика", genres.get(0).getGenre());
        assertEquals("Боевик", genres.get(1).getGenre());
    }
}