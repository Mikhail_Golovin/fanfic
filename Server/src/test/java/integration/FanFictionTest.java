package integration;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.golovin.server.ServerApplication;
import com.golovin.server.Services.*;
import com.golovin.server.classes.*;
import com.golovin.server.models.*;

import com.golovin.server.utils.MyCommon;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


//integration tests
public class FanFictionTest {

    private final ServerApplication serverApplication = new ServerApplication();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private static boolean isPresentBook(int chapterId, Books books) {
        boolean isPresent = false;
        List<Chapters> chaptersList = books.getChaptersList();
        for (Chapters cycleChapter : chaptersList) {
            if (cycleChapter.getId() == chapterId) {
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }


    @ParameterizedTest
    @ValueSource(ints = {0, 1001})
    void outOfBoundTest(int charCount) throws JsonProcessingException {
        String anno = "a".repeat(charCount);
        BookService bookService = MyCommon.getBookService();
        BookClass bookClass = new BookClass("Test Book",
                2, anno, 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), 18, false);
        String bookJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookClass);
        int bookId = serverApplication.changeBook(4, bookJson);
        Books book = bookService.getById(4);
        assertEquals(bookId, -1);
        assertNotEquals(book.getAnnotation(), bookClass.getAnnotation());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 999, 1000})
    void inBoundTest(int charCount) throws JsonProcessingException {
        String anno = "a".repeat(charCount);
        BookService bookService = MyCommon.getBookService();
        BookClass bookClass = new BookClass("Test Book",
                2, anno, 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), 4, false);
        String bookJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookClass);
        int bookId = serverApplication.changeBook(4, bookJson);
        Books book = bookService.getById(4);
        assertEquals(bookId, book.getId());
        assertEquals(book.getAnnotation(), bookClass.getAnnotation());
    }


    @RepeatedTest(3)
    void equivalenceInBounds() throws JsonProcessingException {
        int min = 1;
        int max = 1000;
        String anno = "a".repeat((int) (Math.random() * (max - min)) + min);
        BookService bookService = MyCommon.getBookService();
        BookClass bookClass = new BookClass("Test Book",
                2, anno, 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), 4, false);
        String bookJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookClass);
        int bookId = serverApplication.changeBook(4, bookJson);
        Books book = bookService.getById(4);
        assertEquals(bookId, book.getId());
        assertEquals(book.getAnnotation(), bookClass.getAnnotation());
    }


    @Test
    void getChaptersTest() {
        ListChapterInBook chaptersInBook = MyCommon.getBookService().getChaptersOfBook(1);
        List<Chapters> chaptersList = MyCommon.getChapterService().findAll();
        List<Chapters> bookChaptersList = new ArrayList<>();
        for (Chapters chapter : chaptersList) {
            if (chapter.getBook().getId() == 1) {
                bookChaptersList.add(chapter);
            }
        }
        chaptersInBook.getChapters().sort(Comparator.comparing(ChaptersInBook::getChapterId));
        bookChaptersList.sort(Comparator.comparing(Chapters::getId));
        assertEquals(chaptersInBook.getChapters().size(), bookChaptersList.size());
        for (int i = 0; i < bookChaptersList.size(); i++) {
            assertEquals(bookChaptersList.get(i).getId(), chaptersInBook.getChapters().get(i).getChapterId());
            assertEquals(bookChaptersList.get(i).getNumber(), chaptersInBook.getChapters().get(i).getNumberInBook());
            assertEquals(bookChaptersList.get(i).getName(), chaptersInBook.getChapters().get(i).getName());
        }
    }


    @Test
    void changeTextInChapter() throws JsonProcessingException {
        ChaptersService chaptersService = MyCommon.getChapterService();
        Chapters chapterToChange = chaptersService.getById(1);
        Chapter chapter = new Chapter(chapterToChange.getName(),
                "Дам, Просто умрите... Возможно в следующей жизни повезет больше.");
        String chapterJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(chapter);
        boolean isChanged = serverApplication.changeTextInChapter(1, chapterJson);
        assertTrue(isChanged);
        chapterToChange = chaptersService.getById(1);
        assertEquals(chapter.getText(), chapterToChange.getText());
        assertEquals(chapter.getName(), chapterToChange.getName());

        chapter.setText("Да, Просто умрите... Возможно в следующей жизни повезет больше.");

        isChanged = chaptersService.changeTextInChapter(chapterToChange.getId(), chapter);
        chapterToChange = chaptersService.getById(1);
        assertTrue(isChanged);
        assertEquals(chapter.getText(), chapterToChange.getText());
        assertEquals(chapter.getName(), chapterToChange.getName());
    }


    @Test
    void deleteChapter() {
        int userId = 2;
        int bookId = 1;
        boolean isPresent;

        ChaptersService chaptersService = MyCommon.getChapterService();
        UserService userService = MyCommon.getUserService();

        Chapter chapterToAdd = new Chapter("TestName", "TestText");
        chaptersService.addChapter(bookId, chapterToAdd);

        ListChapterInBook chaptersId = MyCommon.getBookService().getChaptersOfBook(bookId);
        int chapterId = chaptersId.getChapters().get(chaptersId.getChapters().size() - 1).getChapterId();
        userService.changeLastChapter(userId, chapterId);

        boolean deleted = serverApplication.deleteChapter(chapterId);
        assertTrue(deleted);

        Chapters firstChapterOfBook = chaptersService.findFirstChapter(bookId);
        int lastChapterIdAfterDelete = userService.getLastChapter(userId, bookId);
        assertEquals(firstChapterOfBook.getId(), lastChapterIdAfterDelete);

        Books book = MyCommon.getBookService().getById(bookId);

        isPresent = isPresentBook(chapterId, book);
        assertFalse(isPresent);

        List<Chapters> chaptersList = chaptersService.findAll();
        for (Chapters chapter : chaptersList) {
            if (chapter.getId() == chapterId) {
                isPresent = true;
                break;
            }
        }
        assertFalse(isPresent);
    }


    @Test
    void changeLastBook() {
        MyCommon.getBookService().getBook(2, 2);
        List<LastBook> lastBooks = MyCommon.getLastBookService().getLastBook(2);
        lastBooks.sort(Comparator.comparing(LastBook::getPriority));
        assertEquals(2, lastBooks.get(lastBooks.size() - 1).getBook().getId());
    }

    @Test
    void AllBooksWithGenreTest() throws JsonProcessingException {
        String genreBook = serverApplication.getGenreBook(1);
        ListBooksId bookIdList = objectMapper.readValue(genreBook, ListBooksId.class);
        for (int i = 0; i < bookIdList.getBooksId().size(); i++) {
            Books book = MyCommon.getBookService().getById(bookIdList.getBooksId().get(i));
            assertEquals(1, book.getGenre().getId());
        }
    }

    @Test
    void BookSearchTest() throws JsonProcessingException {
        SearchParamsClass searchParams = new SearchParamsClass("поиск", 2, 1);
        String paramsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(searchParams);
        String librarySearch = serverApplication.search(paramsJson);
        ListBooksId bookIdList = objectMapper.readValue(librarySearch, ListBooksId.class);
        List<Books> bookList = MyCommon.getBookService().findAll();
        List<Integer> idList = new ArrayList<>();
        for (Books book : bookList) {
            if ((book.getName().contains(searchParams.getText()))
                    && book.getGenre().getId() == searchParams.getGenreId()) {
                idList.add(book.getId());
            }
        }
        bookIdList.getBooksId().sort(Integer::compareTo);
        idList.sort(Integer::compareTo);
        assertEquals(bookIdList.getBooksId().size(), idList.size());
        for (int i = 0; i < idList.size(); i++) {
            assertEquals(idList.get(i), bookIdList.getBooksId().get(i));
        }
    }

    @Test
    void BookSearchInLibraryTest() throws JsonProcessingException {
        SearchParamsClass searchParams = new SearchParamsClass("Жизнь", 3, 1);
        String paramsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(searchParams);
        String librarySearch = serverApplication.librarySearch(paramsJson, 2);
        ListBooksId bookFoundIdList = objectMapper.readValue(librarySearch, ListBooksId.class);
        ListBooksId libraryIdList = MyCommon.getLibraryService().getAllBooksFromLibrary(2);
        bookFoundIdList.getBooksId().sort(Integer::compareTo);
        libraryIdList.getBooksId().sort(Integer::compareTo);

        Books book;

        for (int i = 0; i < bookFoundIdList.getBooksId().size(); i++) {
            book = MyCommon.getBookService().getById(bookFoundIdList.getBooksId().get(i));
            assertTrue(book.getName().contains("Жизнь"));
        }

        int countOfBooks = 0;

        for (int i = 0; i < libraryIdList.getBooksId().size(); i++) {
            book = MyCommon.getBookService().getById(bookFoundIdList.getBooksId().get(i));
            if(book.getName().contains("Жизнь")) {
                countOfBooks++;
            }
            assertEquals(countOfBooks, bookFoundIdList.getBooksId().size());
        }
    }


    @Test
    void isDraftBook() {
        boolean isDraft = serverApplication.setDraft(2);
        Books book = MyCommon.getBookService().getById(2);
        assertEquals(isDraft, book.isDraft());
        assertTrue(isDraft);
    }


    @Test
    void registerUserTest() {
        int userId = serverApplication.getAuthUserId("TestName", "TestSurname",
                "TestAuth");
        List<User> userList = MyCommon.getUserService().findAll();
        List<Integer> userIdList = new ArrayList<>();
        for (User user : userList) {
            if (user.getName().equals("TestName") && user.getSurname().equals("TestSurname")
                    && user.getGoogle_auth().equals("TestAuth")) {
                userIdList.add(user.getId());
            }
        }
        assertEquals(1, userIdList.size());
        assertEquals(userIdList.get(0), userId);
    }

//    @Test
//    void addCoverTest() throws IOException {
//        byte[] inputArray = "test String".getBytes();
//        MultipartFile multipartFile = new MockMultipartFile("templFile", inputArray);
//        int coverId = serverApplication.addCover(multipartFile);
//
//        CoverService coverService = new CoverService();
//        List<Cover> coverList = coverService.findAll();
//
//        coverList.sort(Comparator.comparingInt(Cover::getId));
//        assertEquals(coverId, coverList.get(coverList.size() - 1).getId());
//
//        assertArrayEquals(inputArray, Files.readAllBytes(Paths.get(coverService.getById(coverId).getDate())));
//    }


}
