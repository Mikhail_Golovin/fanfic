package com.golovin.server.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.golovin.server.classes.*;


public class Service {

    public static String write(ListPresentBookInGenre bookAndGenresList) {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(bookAndGenresList);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(ListBooksId booksId){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(booksId);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(ListChapterInBook listChapterInBook){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(listChapterInBook);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(BookClass book){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(book);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(MainPageBook book){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(book);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(LibraryBook book) {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(book);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(UserClass user){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(user);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(SearchBook book){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(book);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String write(ChapterClass chapter){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(chapter);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static SearchParamsClass readSearchParams(String searchJson) {
        SearchParamsClass searchParamsClass;
        try {
            searchParamsClass = new ObjectMapper().readValue(
                    searchJson,
                    SearchParamsClass.class
            );
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return searchParamsClass;
    }

    public static BookClass readBookClass(String bookJson) {
        BookClass bookClass;
        try {
            bookClass = new ObjectMapper().readValue(
                    bookJson,
                    BookClass.class
            );
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return bookClass;
    }

    public static Chapter readChapter(String chapterJson) {
        Chapter chapter;
        System.out.println(chapterJson);
        try {
            chapter = new ObjectMapper().readValue(
                    chapterJson,
                    Chapter.class
            );
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return chapter;
    }

}