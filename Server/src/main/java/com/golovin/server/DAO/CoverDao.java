package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.CoverDaoInterface;
import com.golovin.server.models.Books;
import com.golovin.server.models.Cover;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class CoverDao implements CoverDaoInterface<Cover, Integer> {

    private Session currentSession;

    private Transaction currentTransaction;

    public CoverDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public void closeCurrentSessionWithTransaction(Session session) {
        session.getTransaction().commit();
        session.close();
    }


    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Session session, Cover cover) {
        session.save(cover);
    }

    @Override
    public void update(Session session, Cover cover) {
        session.update(cover);
    }

    @Override
    public Cover findById(Session session, Integer id) {
        Cover cover = (Cover) session.get(Cover.class, id);
        return cover;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Cover> findAll(Session session) {
        return (List<Cover>) session.createQuery("FROM Cover").list();
    }


}
