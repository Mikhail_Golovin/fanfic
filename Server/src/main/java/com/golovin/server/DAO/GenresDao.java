package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.GenresDaoInterface;
import com.golovin.server.models.Genres;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class GenresDao implements GenresDaoInterface<Genres, Integer> {

    private Session currentSession;

    private Transaction currentTransaction;

    public GenresDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }
    public void closeCurrentSession(Session session) {
        session.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public Genres findById(Session session, Integer id) {
        return session.get(Genres.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Genres> findAll(Session session) {
        return (List<Genres>) session.createQuery("FROM Genres").list();
    }

}
