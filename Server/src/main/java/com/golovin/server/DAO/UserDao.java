package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.UserDaoInterface;
import com.golovin.server.models.User;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class UserDao implements UserDaoInterface<User, Integer> {
    private Session currentSession;

    private Transaction currentTransaction;

    public UserDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public void closeCurrentSessionWithTransaction(Session session) {
        session.getTransaction().commit();
        session.close();
    }


    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Session session, User user) {
        session.saveOrUpdate(user);
    }

    @Override
    public void update(Session session, User user) {
        session.update(user);
    }

    @Override
    public User findById(Session session, Integer id) {
        User user = session.get(User.class, id);
        return user;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> findAll(Session session) {
        return (List<User>) session.createQuery("FROM User").list();
    }

    @Override
    public Integer findUserAuth(Session session, User user) {
        String queryString = "from User where name = :name and surname = :surname and google_auth = :googleAuth";
        Query query = session.createQuery(queryString);
        query.setParameter("name", user.getName());
        query.setParameter("surname", user.getSurname());
        query.setParameter("googleAuth", user.getGoogle_auth());

        if (query.list().isEmpty()){
            return null;
        };
        User sqlUser = (User) query.getSingleResult();
        return sqlUser.getId();
    }

}
