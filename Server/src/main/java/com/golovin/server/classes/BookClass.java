package com.golovin.server.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BookClass {
    public BookClass(String name, int authorId, String annotation,
                     int genreId, int completenessId, int views, String date, int coverId, boolean isDraft) {
        this.name = name;
        this.authorId = authorId;
        this.annotation = annotation;
        this.genreId = genreId;
        this.completenessId = completenessId;
        this.views = views;
        this.date = date;
        this.coverId = coverId;
        this.isDraft = isDraft;
    }


    public BookClass() {
    }

    @JsonProperty
    private String name;

    @JsonProperty
    private int authorId;

    @JsonProperty
    private String annotation;
    @JsonProperty
    private int genreId;

    @JsonProperty
    private int completenessId;

    @JsonProperty
    private int views;

    @JsonProperty
    private String date;

    @JsonProperty
    public int getViews() {
        return views;
    }

    @JsonProperty
    public void setViews(int views) {
        this.views = views;
    }

    @JsonProperty
    private int coverId;

    @JsonProperty
    private boolean isDraft;

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public int getAuthorId() {
        return authorId;
    }

    @JsonProperty
    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    @JsonProperty
    public String getAnnotation() {
        return annotation;
    }

    @JsonProperty
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @JsonProperty
    public int getGenreId() {
        return genreId;
    }

    @JsonProperty
    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    @JsonProperty
    public int getCompletenessId() {
        return completenessId;
    }

    @JsonProperty
    public void setCompletenessId(int completenessId) {
        this.completenessId = completenessId;
    }

    @JsonProperty
    public String getDate() {
        return date;
    }

    @JsonProperty
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty
    public int getCoverId() {
        return coverId;
    }

    @JsonProperty
    public void setCoverId(int coverId) {
        this.coverId = coverId;
    }

    @JsonProperty
    public boolean getIsDraft() {
        return isDraft;
    }

    @JsonProperty
    public void setIsDraft(boolean draft) {
        isDraft = draft;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }


}
