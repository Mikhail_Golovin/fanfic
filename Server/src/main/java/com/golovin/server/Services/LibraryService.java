package com.golovin.server.Services;

import com.golovin.server.DAO.LibraryDao;
import com.golovin.server.classes.ListBooksId;
import com.golovin.server.classes.SearchParamsClass;
import com.golovin.server.models.*;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class LibraryService {
    private static LibraryDao libraryDao = new LibraryDao();

    public LibraryService() {
    }

    public void delete(Library library) {
        Session session = libraryDao.openCurrentSessionWithTransaction();
        libraryDao.delete(session, library);
        libraryDao.closeCurrentSessionWithTransaction(session);
    }

    public void persist(Library library) {
        Session session = libraryDao.openCurrentSession();
        session.beginTransaction();
        libraryDao.persist(session, library);
        session.getTransaction().commit();
        session.close();
    }

    public List<Library> getBooksFromLibraryWithStatus(int statusId, int userId) {
        Session session = libraryDao.openCurrentSession();
        List<Library> books = libraryDao.getBooksWithStatus(session, statusId,userId);
        libraryDao.closeCurrentSession(session);
        return books;

    }

    public ListBooksId getAllBooksFromLibrary(int userId) {
        Session session = libraryDao.openCurrentSession();
        List<Library> library = libraryDao.getAllBooks(session, userId);
        ListBooksId booksId = new ListBooksId();
        for (Library books: library ) {
            booksId.getBooksId().add(books.getBook().getId());
        }
        libraryDao.closeCurrentSession(session);
        return booksId;

    }

    public int getUserBookStatus(int userId, int bookId) {
        Session session = libraryDao.openCurrentSession();
        Library book = libraryDao.getUserBookStatus(session, userId,bookId);
        libraryDao.closeCurrentSession(session);
        if(book == null){
            return 5;
        }
        return book.getStatus().getId();
    }

    public int addBookInLibrary(int userId, int statusId, int bookId) {
        Session session = libraryDao.openCurrentSession();
        Library library = libraryDao.addBookInLibrary(session, userId,statusId, bookId);
        session.close();
        Status status = MyCommon.getStatusService().getById(statusId);
        Books book = MyCommon.getBookService().getById(bookId);
        User user = MyCommon.getUserService().getById(userId);
        if (library != null && library.getStatus().getId() == statusId)
        {
            return statusId;
        }
        else if (library != null){
            delete(library);
        }
        Library newLibrary = new Library(new LibraryPK(bookId,statusId,userId),book,status,user);
        persist(newLibrary);

        return statusId;
    }

    public ListBooksId searchBooks(SearchParamsClass searchParamsClass,int userId){
        Session session = libraryDao.openCurrentSession();
        List<Library> library = libraryDao.searchBooks(session, searchParamsClass.getText(), searchParamsClass.getGenreId(), searchParamsClass.getSortingType(), userId);
        libraryDao.closeCurrentSession(session);
        ListBooksId listBooksId = new ListBooksId();
        for (Library book: library) {
            listBooksId.getBooksId().add(book.getBook().getId());

        }
        return listBooksId;
    }

}
