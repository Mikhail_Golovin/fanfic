package com.golovin.server.Services;

import com.golovin.server.DAO.ChaptersDao;
import com.golovin.server.classes.Chapter;
import com.golovin.server.classes.ChapterClass;
import com.golovin.server.models.Books;
import com.golovin.server.models.Chapters;
import com.golovin.server.models.User;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChaptersService {
    private static ChaptersDao chaptersDao = new ChaptersDao();

    public ChaptersService() {
    }

    public Chapters getById(int id) {
        Session session = chaptersDao.openCurrentSession();
        Chapters chapter = chaptersDao.findById(session, id);
        chaptersDao.closeCurrentSession(session);
        return chapter;
    }


    private void persist(Chapters chapter) {
        Session session = chaptersDao.openCurrentSessionWithTransaction();
        chaptersDao.persist(session, chapter);
        chaptersDao.closeCurrentSessionWithTransaction(session);
    }

    public void update(Chapters chapter) {
        Session session = chaptersDao.openCurrentSessionWithTransaction();
        chaptersDao.update(session, chapter);
        chaptersDao.closeCurrentSessionWithTransaction(session);
    }

    private void delete(int id) {
        Chapters chapter = getById(id);
        Session session = chaptersDao.openCurrentSessionWithTransaction();
        chaptersDao.delete(session, chapter);
        chaptersDao.closeCurrentSessionWithTransaction(session);
    }

    public List<Chapters> findAll() {
        Session session = chaptersDao.openCurrentSession();
        List<Chapters> Chapters = chaptersDao.findAll(session);
        chaptersDao.closeCurrentSession(session);
        return Chapters;
    }

    public ChapterClass findChapterOfBook(int chapterId) {
        Session session = chaptersDao.openCurrentSession();
        Chapters chapter = chaptersDao.findChapterOfBook(session, chapterId);
        ChapterClass chapterClass = new ChapterClass(chapter.getId(), chapter.getText());
        chaptersDao.closeCurrentSession(session);
        return chapterClass;
    }


    public Chapters findLastChapter(int bookId) {
        Session session = chaptersDao.openCurrentSession();
        Chapters chapter = chaptersDao.findLastChapter(session, bookId);
        chaptersDao.closeCurrentSession(session);
        return chapter;
    }

    public Chapters findFirstChapter(int bookId) {
        Session session = chaptersDao.openCurrentSession();
        Chapters chapter = chaptersDao.findFirstChapter(session, bookId);
        chaptersDao.closeCurrentSession(session);
        return chapter;
    }


    public int addChapter(int bookId, Chapter chapterJson) {
        Books book = MyCommon.getBookService().getById(bookId);
        Chapters chapter = findLastChapter(bookId);
        Chapters insertChapter;
        if(chapter == null){
            insertChapter = new Chapters(chapterJson.getName(), 0, chapterJson.getText(), book);
        }else {
            insertChapter = new Chapters(chapterJson.getName(), chapter.getNumber() + 1, chapterJson.getText(), book);
        }
        book.setDate(Date.valueOf(LocalDate.now()));
        MyCommon.getBookService().update(book);
        persist(insertChapter);
        return insertChapter.getId();
    }

    public boolean changeTextInChapter(int chapterId, Chapter chapterJson){
        Chapters chapter = getById(chapterId);
        chapter.setName(chapterJson.getName());
        chapter.setText(chapterJson.getText());
        Books book = chapter.getBook();
        book.setDate(Date.valueOf(LocalDate.now()));
        MyCommon.getBookService().update(book);
        update(chapter);
        return true;
    }

    public boolean deleteChapter(int chapterId){
        try {
            Map<Integer, Integer> userChapter = new HashMap<>();
            Chapters chapter = getById(chapterId);
            Books book = chapter.getBook();
            book.removeChapter(chapter);
            book.setDate(Date.valueOf(LocalDate.now()));
            MyCommon.getBookService().update(book);
            List<User> userList = chapter.getUserList();
            for (User user : userList) {
                List<Chapters> chaptersList = user.getUserChaptersList();
                for (Chapters chapterCheck : chaptersList) {
                    if (chapterCheck.getId() == chapterId) {
                        userChapter.put(user.getId(), chapter.getId());
                    }
                }
            }

            for (var entry : userChapter.entrySet()) {
                User user = MyCommon.getUserService().getById(entry.getKey());
                Chapters chapters = MyCommon.getChapterService().getById(entry.getValue());
                user.removeChapter(chapters);
                MyCommon.getUserService().update(user);
            }

            delete(chapterId);
        } catch (Exception e){
            return false;
        }

        return true;
    }
}
