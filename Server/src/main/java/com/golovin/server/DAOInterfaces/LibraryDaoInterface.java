package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface LibraryDaoInterface<T, Id extends Serializable> {
    public void persist(Session session, T entity);

    public void delete(Session session, T entity);

    public List<T> getBooksWithStatus(Session session, Id id1, Id id2);

    public List<T> getAllBooks(Session session, Id id);

    public T getUserBookStatus(Session session, Id id1, Id id2);

    public T addBookInLibrary(Session session, Id id1, Id id2, Id id3);

    public List<T> searchBooks(Session session, String text, Id id1, Id type, Id id2);
}
