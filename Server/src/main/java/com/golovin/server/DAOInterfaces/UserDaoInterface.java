package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface UserDaoInterface<T, Id extends Serializable> {
    public void persist(Session session, T entity);

    public void update(Session session, T entity);

    public T findById(Session session, Id id);

    public List<T> findAll(Session session);

    public Id findUserAuth(Session session, T entity);
}
