package com.golovin.server.DAOInterfaces;

import com.golovin.server.models.Books;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface BookDaoInterface<T, Id extends Serializable> {

    public void persist(Session session, T entity);

    public void update(Session session, T entity);

    public void delete(Session session, T entity);

    public T findById(Session session, Id id);

    public List<T> findAll(Session session);

    public boolean isGenrePresent(Session session, Id id);

    public T getUserBook(Session session, Id id1, Id id2);

    public List<T> getAuthor(Session session, Id id);

    public List<T> getGenre(Session session, Id id);

    public List<T> getBookInSection(Session session, Id id);

    public List<T> searchBooks(Session session, String text, Id id1, Id type);
}