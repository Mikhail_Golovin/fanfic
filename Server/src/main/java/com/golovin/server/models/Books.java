package com.golovin.server.models;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "книги")
@EqualsAndHashCode
public class Books {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "название")
    private String name;

    @ManyToOne(optional = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "автор")
    private User author;

    @Column(name = "аннотация")
    private String annotation;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "жанр")
    private Genres genre;

    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    @JoinColumn(name = "завершённость")
    private Completeness completeness;

    @Column(name = "просмотры")
    private int views;

    @Column(name = "дата_обновления")
    private Date date;

    @OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "обложка")
    private Cover cover;

    @Column(name = "черновик")
    private boolean isDraft;

    @OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Chapters> chaptersList;


    public void addChapter(Chapters chapter) {
        chapter.setBook(this);
        chaptersList.add(chapter);
    }

    public void removeChapter(Chapters chapter) {
        chaptersList.remove(chapter);
    }

    public Books(User user, String name, String annotation, Genres genre, Completeness completeness,
                 Cover cover, int views, Date date, boolean isDraft) {
        this.author = user;
        this.name = name;
        this.annotation = annotation;
        this.genre = genre;
        this.completeness = completeness;
        this.views = views;
        this.date = date;
        this.cover = cover;
        this.isDraft = isDraft;
        chaptersList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Books{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author=" + author.getId() +
                ", annotation='" + annotation + '\'' +
                ", genre=" + genre.getId() +
                ", completeness=" + completeness.getId() +
                ", views=" + views +
                ", date=" + date.toString() +
                ", cover=" + cover.getId() +
                ", isDraft=" + isDraft +
                '}';
    }

    public Books() {
        chaptersList = new ArrayList<>();
    }
}
