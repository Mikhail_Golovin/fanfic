package com.golovin.server;

import com.golovin.server.Services.*;
import com.golovin.server.utils.MyCommon;
import com.golovin.server.utils.Service;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static com.golovin.server.utils.Service.*;

@SpringBootApplication
@RestController
@RequestMapping("/api")
public class ServerApplication {
    public static final String REAL_PATH_TO_UPLOADS = "C:/Cover/";

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ServerApplication.class);
        Map<String, Object> map = new HashMap<>();
        map.put("server.port", "8280");
        map.put("server.host", getLocalAddress());
        app.setDefaultProperties(map);
        app.run(args);
    }

    public static String getLocalAddress() {
        String localAddress;
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress("google.com", 80));
            localAddress = String.valueOf(socket.getLocalAddress());
        } catch (IOException e) {
            System.out.println(e);
            System.out.println(e.getMessage());
            localAddress = "192.168.88.174";
        }
        System.out.println(localAddress);
        return localAddress;
    }

    @GetMapping("/present_book_in_genre")
    public String getPresentBookInGenre() {
        System.out.println("getPresentBookInGenre");
        BookService bookService = new BookService();
        return Service.write(bookService.getPresentGenres());
    }

    @GetMapping("/library_book")
    public String getLibraryBook(@RequestParam("id_user") int userId,
                                 @RequestParam("id_book") int bookId) {
        System.out.println("getLibraryBook id_user = " + userId + " id_book = " + bookId);
        BookService bookService = new BookService();
        return Service.write(bookService.getLibraryBook(userId, bookId));
    }

    @GetMapping("/book")
    public String getBook(@RequestParam("id_book") int bookId,
                          @RequestParam("id_user") int userId) {
        BookService bookService = new BookService();
        System.out.println("getBook id_book = " + bookId + " id_user = " + userId);
        return Service.write(bookService.getBook(bookId, userId));
    }

    @GetMapping("/main_page_book")
    public String getMainPageBook(@RequestParam("id_book") int bookId) {
        System.out.println("getMainPageBook id_book = " + bookId);
        BookService bookService = new BookService();
        return Service.write(bookService.getMainPageBook(bookId));
    }

    @GetMapping("/find_book")
    public String getSearchBook(@RequestParam("id_book") int bookId,
                                @RequestParam("sort_type") int sortType) {
        System.out.println("getSearchBook id_book = " + bookId);
        BookService bookService = new BookService();
        return Service.write(bookService.getSearchBook(bookId, sortType));
    }

    @GetMapping("/user")
    public String getUser(@RequestParam("id_user") int userId) {
        System.out.println("getUser id_user = " + userId);
        UserService userService = new UserService();
        return Service.write(userService.getUser(userId));
    }

    @GetMapping("/my_library")
    public String getLibrary(@RequestParam("id_user") int userId) {
        System.out.println("getLibrary id_user = " + userId);
        LibraryService libraryService = new LibraryService();
        return Service.write(libraryService.getAllBooksFromLibrary(userId));
    }

    @GetMapping("/my_write_book")
    public String getWriteBook(@RequestParam("id_user") int userId) {
        System.out.println("getWriteBook id_user = " + userId);
        BookService bookService = new BookService();
        return Service.write(bookService.getAuthorBooks(userId));
    }

    @GetMapping("/book_with_status")
    public String getWithStatus(@RequestParam("id_user") int userId,
                                @RequestParam("id_status") int statusId) {
        System.out.println("getWithStatus id_user = " + userId + " id_status = " + statusId);
        BookService bookService = new BookService();
        return Service.write(bookService.getBooksFromLibraryWithStatus(userId, statusId));
    }

    @GetMapping("/book_in_genre")
    public String getGenreBook(@RequestParam("id_genre") int genreId) {
        System.out.println("getGenreBook id_genre = " + genreId);
        BookService bookService = new BookService();
        return Service.write(bookService.getGenreBooks(genreId));
    }


    @GetMapping("/chapters")
    public String getChapters(@RequestParam("id_book") int bookId) {
        System.out.println("getChapters id_book = " + bookId);
        BookService bookService = new BookService();
        return Service.write(bookService.getChaptersOfBook(bookId));
    }

    @GetMapping("/chapter")
    public String getChapter(@RequestParam("id_user") int userId,
                             @RequestParam("id_chapter") int chapterId) {
        System.out.println("getChapter id_user = " + userId + " id_chapter = " + chapterId);
        ChaptersService chaptersService = MyCommon.getChapterService();
        UserService userService = MyCommon.getUserService();
        userService.changeLastChapter(userId, chapterId);
        return Service.write(chaptersService.findChapterOfBook(chapterId));
    }

    @PostMapping("/get_auth_user_id")
    public int getAuthUserId(@RequestParam("first_name") String name,
                             @RequestParam("last_name") String surname,
                             @RequestParam("email") String googleAuth) {
        System.out.println("getAuthUserId first_name = " + name + " last_name = " + surname
                + "email = " + googleAuth);
        UserService userService = new UserService();
        return userService.findUserAuth(name, surname, googleAuth);
    }

    @GetMapping("/book_in_section")
    public String getBooksInSection(@RequestParam("id_section") int sectionId) {
        System.out.println("getBooksInSection id_section = " + sectionId);
        BookService bookService = new BookService();
        return Service.write(bookService.getBookInSection(sectionId));
    }

    @GetMapping("/add_book_in_library")
    public int addBookInLibrary(@RequestParam("id_user") int userId,
                                @RequestParam("id_status") int statusId,
                                @RequestParam("id_book") int bookId) {
        System.out.println("addBookInLibrary id_user = " + userId + " id_status = " + statusId
                + "id_book = " + bookId);
        LibraryService libraryService = new LibraryService();
        return libraryService.addBookInLibrary(userId, statusId, bookId);
    }

    @PostMapping("/add_book")
    public int addBook(@RequestParam("book") String bookJson) {
        System.out.println("addBook book = " + bookJson);
        return new BookService().addBook(readBookClass(bookJson));
    }

    @PostMapping("/add_chapters")
    public int addChapter(@RequestParam("id_book") int bookId,
                          @RequestParam("chapter") String chapterJson) {
        System.out.println("addChapter id_book = " + bookId + " chapter = " + chapterJson);
        ChaptersService chaptersService = new ChaptersService();
        return chaptersService.addChapter(bookId, readChapter(chapterJson));
    }

    @PostMapping("/change_chapter")
    public boolean changeTextInChapter(@RequestParam("id_chapter") int chapterId,
                                       @RequestParam("chapter") String chapterJson) {
        System.out.println("changeTextInChapter id_chapter = " + chapterId + " chapter = " + chapterJson);
        ChaptersService chaptersService = new ChaptersService();
        return chaptersService.changeTextInChapter(chapterId, readChapter(chapterJson));
    }

    @PostMapping("/change_book")
    public int changeBook(@RequestParam("id_book") int bookId,
                          @RequestParam("book") String bookJson) {
        System.out.println("changeBook id_book = " + bookId + " book = " + bookJson);
        BookService bookService = new BookService();
        return bookService.changeBook(bookId, readBookClass(bookJson));
    }

    @PostMapping("/add_cover")
    public int addCover(@RequestParam("picture") MultipartFile file) {
        System.out.println("addCover picture = " + file);
        CoverService coverService = new CoverService();
        if (!file.isEmpty()) {
            try {
                if (!new File(REAL_PATH_TO_UPLOADS).exists()) {
                    new File(REAL_PATH_TO_UPLOADS).mkdir();
                }

                String filePath = REAL_PATH_TO_UPLOADS + "cover" + System.currentTimeMillis() + ".jpg";
                File dest = new File(filePath);
                file.transferTo(dest);
                System.out.println(filePath);
                return coverService.addCover(filePath);

            } catch (Exception e) {
                System.out.println("Вам не удалось загрузить " + " => " + e.getMessage());

            }
        } else {
            System.out.println("Вам не удалось загрузить " + " потому что файл пустой.");
        }
        return -1;

    }

    @PostMapping("/change_cover")
    public boolean changeCover(@RequestParam("id_cover") int coverId,
                               @RequestParam("picture") MultipartFile file) {
        System.out.println("changeCover id_cover = " + coverId + " picture = " + file);
        CoverService coverService = new CoverService();
        if (!file.isEmpty()) {
            try {
                if (!new File(REAL_PATH_TO_UPLOADS).exists()) {
                    new File(REAL_PATH_TO_UPLOADS).mkdir();
                }

                String filePath = coverService.getById(coverId).getDate();
                File dest = new File(filePath);
                file.transferTo(dest);
                System.out.println(filePath);

                return true;

            } catch (Exception e) {
                System.out.println("Вам не удалось загрузить " + " => " + e.getMessage());

            }
        } else {
            System.out.println("Вам не удалось загрузить " + " потому что файл пустой.");
        }
        return false;

    }

    @PostMapping("/search")
    public String search(@RequestParam("search_params") String searchJson) {
        System.out.println("search search_params = " + searchJson);
        return Service.write(new BookService().searchBooks(readSearchParams(searchJson)));
    }

    @PostMapping("/library_search")
    public String librarySearch(@RequestParam("search_params") String searchJson,
                                @RequestParam("id_user") int userId) {
        System.out.println("library_search search_params = " + searchJson + " id_user = " + userId);
        return Service.write(new LibraryService().searchBooks(readSearchParams(searchJson), userId));
    }


    @GetMapping("/delete_chapter")
    public boolean deleteChapter(@RequestParam("id_chapter") int chapterId) {
        System.out.println("deleteChapter id_chapter = " + chapterId);
        ChaptersService chaptersService = new ChaptersService();
        return chaptersService.deleteChapter(chapterId);
    }


    @GetMapping("/set_draft")
    public boolean setDraft(@RequestParam("id_book") int bookId) {
        System.out.println("setDraft id_book = " + bookId);
        BookService bookService = new BookService();
        return bookService.setDraft(bookId);
    }

    @GetMapping("/get_last_chapter")
    public int getLastChapter(@RequestParam("id_user") int userId,
                              @RequestParam("id_book") int bookId) {
        System.out.println("getLastChapter id_user = " + userId + " id_book = " + bookId);
        UserService userService = new UserService();
        return userService.getLastChapter(userId, bookId);
    }

    @GetMapping("/get_last_books")
    public String getLastBooks(@RequestParam("id_user") int userId) {
        System.out.println("getLastBooks id_user = " + userId);
        LastBookService lastBookService = new LastBookService();
        return Service.write(lastBookService.getLastBooks(userId));
    }

    @GetMapping("/get_cover")
    public byte[] getCover(@RequestParam("id_cover") int coverId) {
        System.out.println("getCover id_cover = " + coverId);

        CoverService coverService = new CoverService();
        String path = coverService.getById(coverId).getDate();
        System.out.println(path);
        try {
            return Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
//            throw new RuntimeException(e);
            return new byte[0];
        }
    }


}
