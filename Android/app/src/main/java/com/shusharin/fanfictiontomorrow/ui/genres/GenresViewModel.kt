package com.shusharin.fanfictiontomorrow.ui.genres

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class GenresViewModel(
    api: MyApiServices,
) : ViewModelUpdatable(api) {
    val getPresentBookInGenre = api.getPresentBookInGenre()
    val genres = getPresentBookInGenre.liveGenres
}