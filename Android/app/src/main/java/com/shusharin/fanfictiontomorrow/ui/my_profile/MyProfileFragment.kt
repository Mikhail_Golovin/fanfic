package com.shusharin.fanfictiontomorrow.ui.my_profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.FragmentMyProfileBinding
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment

class MyProfileFragment : MyViewFragment<MyProfileViewModel, FragmentMyProfileBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<MyProfileViewModel>()
        try {
            viewModel.idAnotherUser = arguments?.getInt(getString(R.string.id_user))!!
            if (viewModel.idAnotherUser == 0) {
                viewModel.idAnotherUser = -1
            }
        } catch (e: java.lang.Exception) {
            viewModel.idAnotherUser = -1
        }

        refresh()
        if (viewModel.idAnotherUser == -1) {
            binding.createNewWorks.setOnClickListener {
                val bundle = Bundle()
                bundle.putInt(
                    getString(R.string.id_book),
                    -1
                )
                MainActivity.navigate(R.id.nav_create_and_edit, bundle)
            }
        } else {
            binding.createNewWorks.visibility = View.INVISIBLE
        }

        binding.library.setOnClickListener {
            val bundle = Bundle()
            if (viewModel.idAnotherUser != -1) {
                bundle.putInt(
                    getString(R.string.id_user),
                    viewModel.idAnotherUser
                )
            } else {
                bundle.putInt(
                    getString(R.string.id_user),
                    MainActivity.idUser
                )
            }
            MainActivity.navigate(R.id.nav_my_library, bundle)
        }

        binding.works.setOnClickListener {
            val bundle = Bundle()
            if (viewModel.idAnotherUser != -1) {
                bundle.putInt(
                    getString(R.string.id_user),
                    viewModel.idAnotherUser
                )
            }
            MainActivity.navigate(R.id.nav_my_works, bundle)
        }

        viewModel.person.observe(viewLifecycleOwner) {
            binding.name.text = it.name
            binding.surname.text = it.surname
        }

        update()
        refresh()

        return binding.root
    }

    override fun update() {
        viewModel.getUser.sendRequest(
            if (viewModel.idAnotherUser == -1) {
                MainActivity.idUser
            } else {
                viewModel.idAnotherUser
            }
        )
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentMyProfileBinding.inflate(
            inflater,
            container,
            false
        )
    }

    override fun setSwipeRefreshLayout() {
        swipeRefreshLayout = binding.swipeRefreshLayout
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(MainActivity.Companion.Nav(R.id.nav_my_profile, arguments))
    }

}