package com.shusharin.fanfictiontomorrow.ui.main_page

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.main_page.adapters.AdapterSection
import com.shusharin.fanfictiontomorrow.ui.parents.RecycleViewFragment
import kotlinx.coroutines.*
import java.util.*

class MainPageFragment :
    RecycleViewFragment<MainPageViewModel, AdapterSection.ChapterViewHolder, AdapterSection>() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<MainPageViewModel>()
        refresh()
        return getRoot1()
    }

    private fun getRoot1() = binding.root

    override fun onStart() {
        super.onStart()
        update()
    }

    override fun update() {
        viewModel.getListIdBooksInSection.sendRequest(Sections.POPULATE)
        viewModel.getListIdBooksInSection1.sendRequest(Sections.HOT_NEW_ITEMS)
        if (MainActivity.idUser != -1) {
            viewModel.getListIdLastBooks.sendRequest()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setAdapter() {
        if (MainActivity.idUser == -1) {
            viewModel.sections.remove(Sections.LAST)
        }
        adapter = AdapterSection(viewModel.sections)
        viewModel.liveSearchInSectionBook.observe(this) { populate ->
            viewModel.sections[Sections.POPULATE] = populate
            adapter!!.notifyDataSetChanged()
        }
        viewModel.liveSearchInSectionBook1.observe(this) { hotNewItems ->
            viewModel.sections[Sections.HOT_NEW_ITEMS] = hotNewItems
            adapter!!.notifyDataSetChanged()
        }
        viewModel.liveLastBooks.observe(this) { lastBooks ->
            viewModel.sections[Sections.LAST] = lastBooks
            adapter!!.notifyDataSetChanged()
        }
        list?.setHasFixedSize(true)
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(MainActivity.Companion.Nav(R.id.nav_main_page))
    }
}