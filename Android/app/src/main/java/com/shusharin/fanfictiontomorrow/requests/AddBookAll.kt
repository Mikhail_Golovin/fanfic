package com.shusharin.fanfictiontomorrow.requests

import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class AddBookAll(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
) : ChaptersRequests(owner, api),
    IAddCover,
    IAddBook {
    val liveIsAdded: MutableLiveData<Boolean>
        get() = liveIsSuccess
    var idBook = -1

    fun sendRequest(
        bitmap: Bitmap,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        addCover(bitmap, bookClass, deleteChapters, addedChapters, changesChapters)
    }

    override fun setIdCover(
        idCover: Int,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        bookClass.coverId = idCover
        addBook(bookClass, deleteChapters, addedChapters, changesChapters)
    }

    override fun setIdBook(
        idBook: Int,
        deleteChapters: java.util.HashMap<Int, Chapter>,
        addedChapters: java.util.HashMap<Int, Chapter>,
        changesChapters: java.util.HashMap<Int, Chapter>,
    ) {
        this.idBook = idBook
        deleteChapter(idBook, deleteChapters, addedChapters, changesChapters)
    }
}