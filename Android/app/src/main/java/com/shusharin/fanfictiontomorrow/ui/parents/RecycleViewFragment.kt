package com.shusharin.fanfictiontomorrow.ui.parents

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.databinding.FragmentRecyclerViewBinding

abstract class RecycleViewFragment<
        ViewModel : ViewModelUpdatable,
        E : RecyclerView.ViewHolder?, Adapter : RecyclerView.Adapter<E>,
        > :
    MyViewFragment<ViewModel, FragmentRecyclerViewBinding>() {
    protected var list: RecyclerView? = null
    protected var adapter: Adapter? = null
        set(value) {
            field = value
            list?.adapter = adapter
        }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setList()
        return getRoot()
    }

    private fun getRoot() = binding.root

    private fun setList() {
        list = binding.list
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
    }

    override fun setSwipeRefreshLayout() {
        swipeRefreshLayout = binding.swipeRefreshLayout
    }
}