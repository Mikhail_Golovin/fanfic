package com.shusharin.fanfictiontomorrow.utils

object Common {
    private const val BASE_URL = "http://89.208.197.63:8280/api/"
//    private const val BASE_URL = "http://192.168.88.174:8280/api/"
    val retrofitService: RetrofitServices
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitServices::class.java)

}