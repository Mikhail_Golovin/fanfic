package com.shusharin.fanfictiontomorrow.ui.find_book

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class FindBookViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    var whereFindId: Int = -1
    var idWhereFind: Int = -1
    var text: String = ""
    var idGenre: Int = 1
    var idSorting: Int = 1

    val getListIdBooksInSection = api.getListIdBooksInSection()
    val liveSearchInSectionBook = getListIdBooksInSection.liveSearchInSectionBook

    val getListIdGenreBooks = api.getListIdGenreBooks()
    val liveSearchInGenreBook = getListIdGenreBooks.liveSearchInGenreBook

    val getListIdSearchBooks = api.getListIdSearchBooks()
    val liveSearchBook = getListIdSearchBooks.liveSearchBook

}