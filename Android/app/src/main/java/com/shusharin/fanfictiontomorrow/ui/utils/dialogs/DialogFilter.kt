package com.shusharin.fanfictiontomorrow.ui.utils.dialogs

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusCreatedBooks
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook

class DialogFilter(val supportFragmentManager: FragmentManager, private val fragment: Fragment) :
    DialogFragment() {
    private val nameStatusReadings = ArrayList<String>()
    private val nameGenres = ArrayList<String>()
    private val nameCompletenessS = ArrayList<String>()

    lateinit var dialogStatusReading: DialogSelectionList
    lateinit var dialogGenres: DialogSelectionList
    lateinit var dialogCompleteness: DialogSelectionList

    interface FilterListener {
        fun filterApply(idStatusReading: Int, idGenre: Int, idCompleteness: Int)
    }

    private lateinit var listener: FilterListener

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            // Instantiate the ConfirmationListener so we can send events to the host
            listener = fragment as FilterListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException("$fragment must implement SearchListener")
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        nameGenres.clear()
        nameStatusReadings.clear()
        nameCompletenessS.clear()

        nameGenres.add("Все")
        nameGenres.addAll(Array(Genres.values().size) { i -> requireContext().getString(Genres.values()[i].nameG) })

        nameStatusReadings.add("Все")
        nameStatusReadings.addAll(Array(StatusReadingBook.values().size - 2) { i -> StatusReadingBook.values()[i].nameSRB })

        nameCompletenessS.add("Все")
        nameCompletenessS.addAll(Array(StatusCreatedBooks.values().size) { i ->
            requireContext().getString(
                StatusCreatedBooks.values()[i].nameSCB
            )
        })
//        dialog!!.setMessage("Найдётся не всё)!")
        val viewDialog = inflater.inflate(R.layout.content_filter, null)

        val applyFilters = viewDialog.findViewById<Button>(R.id.apply_filters)

        val nameStatusReading = viewDialog.findViewById<TextView>(R.id.name_status_reading)
        val nameGenre = viewDialog.findViewById<TextView>(R.id.name_genre)
        val nameCompleteness = viewDialog.findViewById<TextView>(R.id.name_commplitness)

        dialogStatusReading = createInnerDialog(nameStatusReading, nameStatusReadings)
        dialogGenres = createInnerDialog(nameGenre, nameGenres)
        dialogCompleteness = createInnerDialog(nameCompleteness, nameCompletenessS)

        applyFilters.setOnClickListener {
            listener.filterApply(
                dialogStatusReading.getIdSelected() - 1,
                dialogGenres.getIdSelected() - 1,
                dialogCompleteness.getIdSelected() - 1
            )
            dismiss()
        }

        return viewDialog
    }

    private fun createInnerDialog(
        nameGenre: TextView,
        arrayListNames: ArrayList<String>,
    ): DialogSelectionList {
        nameGenre.text = arrayListNames[0]
        val dialogNames =
            DialogSelectionList(
                "Выберите жанр",
                Array(arrayListNames.size) { i -> arrayListNames[i] },
                nameGenre
            )
        nameGenre.setOnClickListener {
            dialogNames.show(supportFragmentManager.beginTransaction(), "DialogFilter")
        }
        return dialogNames
    }
}