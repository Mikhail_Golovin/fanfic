package com.shusharin.fanfictiontomorrow.ui.create_and_edit_book

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class CreateAndEditViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    val getBookClass = api.getBookClass()
    val liveBook = getBookClass.liveBook

    val getListIdChapters = api.getListIdChapters()
    val liveChapters = getListIdChapters.liveChapters

    val addBookAll = api.addBookAll()
    val liveIsAdded = addBookAll.liveIsAdded

    val changeBookAll = api.changeBookAll()
    val liveIsChanged = changeBookAll.liveIsChanged
}