package com.shusharin.fanfictiontomorrow.requests.add_single

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import retrofit2.Call

class BookInLibraryParams(val idUser: Int, val idStatus: Int, val idBook: Int) {
}

class AddBookInLibrary(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : MyRequestIdentity<Boolean, BookInLibraryParams>(startRefresh, stopRefresh) {
    val liveIsAdded: MutableLiveData<Boolean>
        get() = liveData

    fun sendRequest(idUser: Int = MainActivity.idUser, idStatus: Int, idBook: Int) {
        sendRequest(BookInLibraryParams(idUser, idStatus, idBook))
    }

    override fun call(params: BookInLibraryParams): Call<Boolean> {
        return service.addBookInLibrary(params.idUser, params.idStatus, params.idBook)
    }

}