package com.shusharin.fanfictiontomorrow.ui.edit_chapter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.FragmentEditChapterBinding
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.create_and_edit_book.CreateAndEditFragment
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment
import java.io.InputStream

class EditChapterFragment : MyViewFragment<EditChapterViewModel, FragmentEditChapterBinding>() {
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<EditChapterViewModel>()
        viewModel.idChapter = arguments?.getInt(getString(R.string.id_chapter))!!

        try {
            viewModel.idBook = arguments?.getInt(getString(R.string.id_book))!!
        } catch (e: Exception) {
            viewModel.idBook = -1
        }

        binding.save.setOnClickListener {
            CreateAndEditFragment.chapters[viewModel.idChapter]!!.text =
                binding.editText.text.toString()
            val bundle = Bundle()
            bundle.putBoolean(
                getString(R.string.is_change_chapter),
                true
            )
            MainActivity.navigate(R.id.nav_create_and_edit, bundle)
        }

        binding.load.setOnClickListener {
            val intent = Intent()
            intent.type = "text/plain"
            intent.action = Intent.ACTION_GET_CONTENT
            resultLauncher.launch(intent)
        }

        binding.editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.chapter.text = p0.toString()
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })



        if (!CreateAndEditFragment.addedChapters.containsKey(viewModel.idChapter)
            && !CreateAndEditFragment.changesChapters.containsKey(
                viewModel.idChapter
            )
        ) {
            update()
            refresh()
        } else {
            postExecuteGetChapter()
        }

        if (!viewModel.isChangeChapter) {
            viewModel.liveChapter.observe(viewLifecycleOwner) {
                viewModel.chapter.text = it.text
                binding.editText.setText(it.text)
                viewModel.isChangeChapter = true
                if (!CreateAndEditFragment.changesChapters.containsKey(viewModel.idChapter)) {
                    if (!CreateAndEditFragment.addedChapters.containsKey(viewModel.idChapter)) {
                        CreateAndEditFragment.changesChapters[viewModel.idChapter] =
                            CreateAndEditFragment.chapters[viewModel.idChapter]!!
                    }
                }
            }
        }
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val selectedFileUri: Uri? = result.data?.data

                val inputStream: InputStream? =
                    selectedFileUri?.let { requireContext().contentResolver.openInputStream(it) }
                if (inputStream != null) {
                    binding.editText.text.append(
                        String(
                            inputStream.readBytes(),
                            charset("CP1251")
                        )
                    )
                    val toast = Toast.makeText(
                        context,
                        "Load file: $selectedFileUri", Toast.LENGTH_LONG
                    )
                    toast.show()
                } else {
                    val toast = Toast.makeText(
                        context,
                        "can't read file" + binding.editText.text, Toast.LENGTH_LONG
                    )
                    toast.show()
                }
            }
        }

    override fun refresh() {
        swipeRefreshLayout.setOnRefreshListener {
            if (viewModel.isChangeChapter) {
                stopRefresh()
            } else {
                update()
            }
        }
    }

    override fun update() {
        if (CreateAndEditFragment.chapters[viewModel.idChapter]!!.text == "") {
            viewModel.getChapter.sendRequest(viewModel.idChapter)
        } else {
            postExecuteGetChapter()
            stopRefresh()
        }
    }

    fun postExecuteGetChapter() {
        viewModel.isChangeChapter = true
        viewModel.chapter = CreateAndEditFragment.chapters[viewModel.idChapter]!!
        binding.editText.setText(viewModel.chapter.text)
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentEditChapterBinding.inflate(inflater, container, false)
    }

    override fun setSwipeRefreshLayout() {
        swipeRefreshLayout = binding.swipeRefreshLayout
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(MainActivity.Companion.Nav(R.id.nav_find_book, arguments))
    }
}