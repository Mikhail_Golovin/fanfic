package com.shusharin.fanfictiontomorrow.ui.my_library.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.main_page.adapters.AdapterSmallBooks

class AdapterReedBooks(private val readBooks: HashMap<Int, ReadBook>) :
    RecyclerView.Adapter<AdapterReedBooks.ReadBookViewHolder>() {

    open class ReadBookViewHolder(itemView: View) :
        AdapterSmallBooks.SmallBookViewHolder(itemView) {
        val status: ImageView = itemView.findViewById(R.id.status)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReadBookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_read_book, parent, false)
        return ReadBookViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ReadBookViewHolder, position: Int) {
        val temp = readBooks.toList().toTypedArray()
        temp.sortBy { it.first }
//        println(temp.contentToString())
        val readBook = temp[position].second

        holder.coverBook.setImageBitmap(readBook.bitmap)

        holder.nameBook.text = readBook.name
        holder.nameAuthor.text = readBook.author.toString()
        holder.cornerCon.clipToOutline = true

        holder.cornerCon.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_book),
                readBook.id
            )
            MainActivity.navigate(R.id.nav_book, bundle)
        }

        holder.coverBook.clipToOutline = true
        holder.status.setImageResource(readBook.statusReadingBook.idIcon)
    }

    override fun getItemCount() = readBooks.size
}