package com.shusharin.fanfictiontomorrow.ui.utils.dialogs

import android.app.Dialog
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.shusharin.fanfictiontomorrow.R


class DialogSelectionList(
    private val title: String,
    private val nameSelectedItems: Array<String>,
    private val nameSelectedItem: TextView,
    private val listener: MyDialogFragmentListener? = null,
    private val isNeedChange: Boolean = true,
) :
    DialogFragment() {
    private var idSelected: Int = 0

    interface MyDialogFragmentListener {
        fun onChangeValue()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            var idSelectedCurrent = idSelected
            setText()
            builder.setTitle(title)
                .setSingleChoiceItems(
                    nameSelectedItems, this.idSelected
                ) { _, item ->
                    idSelectedCurrent = this.idSelected
                    this.idSelected = item
                }
                .setPositiveButton(getString(R.string.dialog_ok)) { _, _ ->
                    listener?.onChangeValue()
                    if (isNeedChange) {
                        idSelectedCurrent = this.idSelected
                        setText()
                    } else {
//                        this.idSelected = idSelectedCurrent
                    }
                }
                .setNegativeButton(
                    "Отмена"
                ) { _, _ ->
                    this.idSelected = idSelectedCurrent
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    fun setIdSelected(id: Int) {
        idSelected = id - 1
        setText()
    }

    private fun setText() {
        nameSelectedItem.text = nameSelectedItems[this.idSelected]
    }

    fun getIdSelected(): Int {
        return idSelected + 1
    }
}