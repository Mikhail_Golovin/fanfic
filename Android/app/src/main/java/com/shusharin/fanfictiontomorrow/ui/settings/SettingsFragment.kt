package com.shusharin.fanfictiontomorrow.ui.settings

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate.*
import androidx.navigation.findNavController
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        MainActivity.stack.addLast(
            MainActivity.Companion.Nav(R.id.nav_settings, arguments)
        )

        val themes: ListPreference? = findPreference("themes")
        themes?.setOnPreferenceChangeListener { _, newValue ->
            when (newValue) {
                "dark" -> {
                    setDefaultNightMode(MODE_NIGHT_YES)
                }
                "light" -> {
                    setDefaultNightMode(MODE_NIGHT_NO)
                }
                "system" -> {
                    setDefaultNightMode(MODE_NIGHT_FOLLOW_SYSTEM)
                }
            }
            true
        }
        val sharedPreferences: SharedPreferences? =
            activity?.let { PreferenceManager.getDefaultSharedPreferences(it) }
        val edit = sharedPreferences?.edit()

        val openWindow: ListPreference? = findPreference("open_window")
        openWindow?.setOnPreferenceChangeListener { _, newValue ->
            when (newValue) {
                "main_page" -> {
                    edit?.putInt(getString(R.string.id_nav), R.id.nav_main_page)
                    edit?.apply()
                }
                "my_library" -> {
                    edit?.putInt(getString(R.string.id_nav), R.id.nav_my_library)
                    edit?.apply()
                }
                "my_profile" -> {
                    edit?.putInt(getString(R.string.id_nav), R.id.nav_my_profile)
                    edit?.apply()
                }
            }
            true
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        MyViewFragment.appBar(
            requireActivity(),
            requireContext(),
            parentFragmentManager,
            viewLifecycleOwner
        )
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onPreferenceTreeClick(preference: Preference): Boolean {
        val key = preference.key
        if (key.equals("info")) {
            requireView().findNavController().navigate(R.id.nav_info)
        }

        return super.onPreferenceTreeClick(preference)
    }

}
