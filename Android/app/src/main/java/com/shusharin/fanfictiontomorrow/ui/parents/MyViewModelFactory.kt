package com.shusharin.fanfictiontomorrow.ui.parents

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.shusharin.fanfictiontomorrow.ui.book.BookViewModel
import com.shusharin.fanfictiontomorrow.ui.chapters.ChaptersViewModel
import com.shusharin.fanfictiontomorrow.ui.create_and_edit_book.CreateAndEditViewModel
import com.shusharin.fanfictiontomorrow.ui.edit_chapter.EditChapterViewModel
import com.shusharin.fanfictiontomorrow.ui.find_book.FindBookViewModel
import com.shusharin.fanfictiontomorrow.ui.genres.GenresViewModel
import com.shusharin.fanfictiontomorrow.ui.login.LoginViewModel
import com.shusharin.fanfictiontomorrow.ui.main_page.MainPageViewModel
import com.shusharin.fanfictiontomorrow.ui.my_library.MyLibraryViewModel
import com.shusharin.fanfictiontomorrow.ui.my_profile.MyProfileViewModel
import com.shusharin.fanfictiontomorrow.ui.my_works.MyWorksViewModel
import com.shusharin.fanfictiontomorrow.ui.read_book.MyReadBookViewModel
import com.shusharin.fanfictiontomorrow.utils.MyApiServices


class MyViewModelFactory(
    private val api: MyApiServices,
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            GenresViewModel::class.java -> GenresViewModel(api) as T
            MyProfileViewModel::class.java -> MyProfileViewModel(api) as T
            BookViewModel::class.java -> BookViewModel(api) as T
            ChaptersViewModel::class.java -> ChaptersViewModel(api) as T
            CreateAndEditViewModel::class.java -> CreateAndEditViewModel(api) as T
            EditChapterViewModel::class.java -> EditChapterViewModel(api) as T
            FindBookViewModel::class.java -> FindBookViewModel(api) as T
            LoginViewModel::class.java -> LoginViewModel(api) as T
            MainPageViewModel::class.java -> MainPageViewModel(api) as T
            MyLibraryViewModel::class.java -> MyLibraryViewModel(api) as T
            MyWorksViewModel::class.java -> MyWorksViewModel(api) as T
            MyReadBookViewModel::class.java -> MyReadBookViewModel(api) as T
            else -> {
                super.create(modelClass)
            }
        }
    }
}
