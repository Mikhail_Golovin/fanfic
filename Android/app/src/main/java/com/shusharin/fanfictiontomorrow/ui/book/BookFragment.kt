package com.shusharin.fanfictiontomorrow.ui.book

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.FragmentBookBinding
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.MainActivity.Companion.createNeedLoginDialog
import com.shusharin.fanfictiontomorrow.ui.MainActivity.Companion.navigate
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment
import com.shusharin.fanfictiontomorrow.ui.utils.WHERE_FIND
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSelectionList


class BookFragment : MyViewFragment<BookViewModel, FragmentBookBinding>(),
    DialogSelectionList.MyDialogFragmentListener {
    lateinit var dialogStatusAddedInLibrary: DialogSelectionList
    val book: Book
        get() = viewModel.liveBook.value!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<BookViewModel>()
        viewModel.idBook = arguments?.getInt(getString(R.string.id_book))!!
        setClipToOutline()


        setOnClickRead()
        setOnClickTableOfContent()
        setOnClickNameAuthor()
        setOnClickChange()
        setOnClickNameGenre()

        dialogStatusAddedInLibrary = DialogSelectionList(
            "Выберите статус добавления в библиотеку",
            viewModel.nameStatusAddedInLibrary,
            getButtonList(), this, false
        )

        setOnClickList()

        viewModel.liveBook.observe(viewLifecycleOwner) { book ->
            println(book)
            if (!book.isDraft || book.author.id == MainActivity.idUser) {
                if (book.author.id != 0) {
                    binding.nameBook.text = book.name

                    binding.stateBook.text =
                        requireContext().getString(book.completeness.nameSCB)
                    binding.stateBook.setTextColor(
                        resources.getColor(
                            book.completeness.idColor,
                            requireContext().theme
                        )
                    )

                    binding.nameGenre.text =
                        requireContext().getText(book.genre.nameG)
                    binding.annotateText.text = book.annotate
                    binding.quantityOfViews.text = book.quantityOfViews.toString()
                    binding.dateUpdate.text = book.date
                    getButtonList().text = book.statusReadingBook.nameSRB
                    binding.cover.setImageBitmap(book.bitmap)

                    if (book.author.id != -1) {
                        if (book.author.id != MainActivity.idUser && book.statusReadingBook.id < 5) {
                            dialogStatusAddedInLibrary.setIdSelected(book.statusReadingBook.id)
                        }

                        binding.nameAuthor.text = book.author.toString()
                        if (book.author.id == MainActivity.idUser) {
                            binding.change.visibility = View.VISIBLE
                        } else {
                            binding.change.visibility = View.INVISIBLE
                        }
                    }
                } else {
                    Toast.makeText(context, getString(R.string.book_not_found), Toast.LENGTH_SHORT)
                        .show()
                }
            } else {
                Toast.makeText(context, "Книга перемещена в черновики", Toast.LENGTH_SHORT)
                    .show()
                navigate(R.id.nav_main_page)
            }
        }

        viewModel.liveIdLastChapter.observe(viewLifecycleOwner) { idLastChapter ->
            viewModel.idLastChapter = idLastChapter
        }

        viewModel.liveIsAdded.observe(viewLifecycleOwner) {
            if (it) {
                dialogStatusAddedInLibrary.setIdSelected(dialogStatusAddedInLibrary.getIdSelected())
            }
            println("liveIsAdded $it")
        }
        getButtonList().text = StatusReadingBook.NOT_IN_LIBRARY.nameSRB

        update()
        refresh()

        return getRoot()
    }

    private fun setOnClickList() {
        getButtonList().setOnClickListener {
            if (MainActivity.idUser != -1) {
                if (book.author.id != MainActivity.idUser) {
                    dialogStatusAddedInLibrary.show(
                        requireActivity().supportFragmentManager.beginTransaction(),
                        "CreateAndEditFragment"
                    )
                }
            } else {
                createNeedLoginDialog(requireContext()).show()
            }
        }
    }

    private fun getButtonList() = binding.lists

    private fun setOnClickNameGenre() {
        binding.nameGenre.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                requireContext().getString(R.string.where_find),
                WHERE_FIND.GENRE.id
            )
            bundle.putInt(
                requireContext().getString(R.string.id_genre),
                book.genre.id
            )
            navigate(R.id.nav_find_book, bundle)
    //            Navigation.createNavigateOnClickListener(R.id.nav_find_book, bundle)
        }
    }

    private fun setOnClickChange() {
        binding.change.setOnClickListener {
            viewModel.setDraft.sendRequest(viewModel.idBook).observe(viewLifecycleOwner) {
                if (it) {
                    val bundle = Bundle()
                    bundle.putInt(
                        getString(R.string.id_book),
                        viewModel.idBook
                    )
                    navigate(R.id.nav_create_and_edit, bundle)
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Не удалось убрать книгу в черновики",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun setOnClickNameAuthor() {
        binding.nameAuthor.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                getString(R.string.id_user),
                book.author.id
            )
            navigate(R.id.nav_my_profile, bundle)
        }
    }

    private fun setOnClickTableOfContent() {
        binding.tableOfContents.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                getString(R.string.id_book),
                viewModel.idBook
            )
            navigate(R.id.nav_chapters, bundle)
        }
    }

    private fun setOnClickRead() {
        getRead().setOnClickListener {
            val bundle = Bundle()

            bundle.putInt(
                getString(R.string.id_book),
                viewModel.idBook
            )
            bundle.putInt(
                getString(R.string.id_chapter),
                viewModel.idLastChapter
            )
            navigate(R.id.nav_read_book, bundle)
        }
    }

    private fun getRead() = binding.read

    private fun setClipToOutline() {
        binding.cover.clipToOutline = true
    }

    private fun getRoot() = binding.root

    override fun update() {
        viewModel.getBookClass.sendRequest(idBook = viewModel.idBook)
        viewModel.getLastChapter.sendRequest(idBook = viewModel.idBook)
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentBookBinding.inflate(inflater, container, false)
    }

    override fun setSwipeRefreshLayout() {
        swipeRefreshLayout = binding.swipeRefreshLayout
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(MainActivity.Companion.Nav(R.id.nav_book, arguments))
    }

    override fun onChangeValue() {
        viewModel.addBookInLibrary.sendRequest(
            idStatus = dialogStatusAddedInLibrary.getIdSelected(),
            idBook = viewModel.idBook
        )
        println("addBookInLibrary")
    }
}