package com.shusharin.fanfictiontomorrow.ui.parents

import androidx.lifecycle.ViewModel
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

abstract class ViewModelUpdatable(val api: MyApiServices) :
    ViewModel() {

    open var idBook: Int = -1
    open var user = Person()
    open var idAnotherUser: Int = -1
    open var idLastChapter = -1
    open val nameStatusAddedInLibrary =
        Array(StatusReadingBook.values().size - 2) { i -> StatusReadingBook.values()[i].nameSRB }
//
//    private var mChapters = MutableLiveData<HashMap<Int, Chapter>>()
//    val chapters: LiveData<HashMap<Int, Chapter>>
//        get() = mChapters
}