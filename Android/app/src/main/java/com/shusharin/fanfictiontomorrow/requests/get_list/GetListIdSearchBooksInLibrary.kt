package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetLibraryBook
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.requests.utils.model.SearchParamsClass
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call

class ListIdSearchBooksInLibraryParams(val searchParamsClass: SearchParamsClass, val idUser: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "ListIdSearchBooksInLibraryParams(searchParamsClass=$searchParamsClass, idUser=$idUser)"
    }
}

class GetListIdSearchBooksInLibrary(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : GetListIdBook<ListIdSearchBooksInLibraryParams, ReadBook>(
    startRefresh,
    stopRefresh
), IGetLibraryBook {
    val liveSearchLibraryBook: MutableLiveData<HashMap<Int, ReadBook>>
        get() = liveData

    fun sendRequest(searchParamsClass: SearchParamsClass, idUser: Int) {
        data.clear()
        sendRequest(ListIdSearchBooksInLibraryParams(searchParamsClass, idUser))
    }

    override fun analysisOfResponseBody(
        responseBody: ListBookId,
        params: ListIdSearchBooksInLibraryParams,
    ) {
        getLibraryBook(params.idUser, responseBody)
    }

    override fun call(params: ListIdSearchBooksInLibraryParams): Call<ListBookId> {
        val requestBody = RequestBody.create(
            MediaType.get("application/json; charset=utf-8"), params.searchParamsClass.toString()
        )
        return service.librarySearch(requestBody, params.idUser)
    }

    override fun setLibraryBook(it: ReadBook) {
        setLiveData(it)
    }
}