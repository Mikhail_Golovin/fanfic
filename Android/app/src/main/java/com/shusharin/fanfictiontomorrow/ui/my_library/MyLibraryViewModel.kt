package com.shusharin.fanfictiontomorrow.ui.my_library

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class MyLibraryViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    var isSearch = false
    var text: String = ""
    var idGenre: Int = 1
    var idSorting: Int = 1

    var filteredStatusReading: Int = 0
    var filteredGenre: Int = 0
    var filteredCompleteness: Int = 0

    val getListIdSearchBooksInLibrary = api.getListIdSearchBooksInLibrary()
    val liveSearchInSectionBook = getListIdSearchBooksInLibrary.liveSearchLibraryBook

    val getListIdBooksLibrary = api.getListIdBooksLibrary()
    val liveLibraryBook = getListIdBooksLibrary.liveLibraryBooks
}