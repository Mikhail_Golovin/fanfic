package com.shusharin.fanfictiontomorrow.requests.add_single

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.RequestCover
import retrofit2.Call

class CoverParams(val bitmap: Bitmap) : MyRequestParams() {
    override fun toString(): String {
        return "CoverParams(bitmap=$bitmap)"
    }
}

class AddCover(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : RequestCover<Int, CoverParams>(startRefresh, stopRefresh) {
    val liveIdCover: MutableLiveData<Int>
        get() = liveData

    fun sendRequest(bitmap: Bitmap) {
        sendRequest(CoverParams(bitmap))
    }

    override fun call(params: CoverParams): Call<Int> {
        val dataMultiPart = convertToPart(params.bitmap)
        return service.addCover(dataMultiPart)
    }


}