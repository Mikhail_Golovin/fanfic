package com.shusharin.fanfictiontomorrow.requests.utils.classes

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.model.*

open class SmallBook : BaseObservable {
    // Главная страница
    var id: Int = -1
    private var observeBitmap = ObservableField<Bitmap>()
    var bitmap: Bitmap
        get() = observeBitmap.get()!!
        set(value) {
            observeBitmap.set(value)
        }

    var idCoverBook: Int = -1
    var name: String = ""
    var author: Person = Person()
    var sort: Long = 0L
    var isDraft: Boolean = false

    constructor(id: Int) {
        this.id = id
    }

    constructor(resources: Resources) {
        observeBitmap.set(BitmapFactory.decodeResource(resources, R.drawable.empty))
    }

    constructor(idBook: Int, mainPageBook: MainPageBook, resources: Resources) {
        this.id = idBook
        this.name = mainPageBook.name
        this.isDraft = mainPageBook.isDraft
        this.idCoverBook = mainPageBook.coverId
        observeBitmap.set(BitmapFactory.decodeResource(resources, R.drawable.empty))
    }

    constructor(
        id: Int,
        bitmap: Bitmap,
        idCoverBook: Int,
        name: String,
        author: Person,
        sort: Long,
        isDraft: Boolean,
    ) : super() {
        this.id = id
        this.bitmap = bitmap
        this.idCoverBook = idCoverBook
        this.name = name
        this.author = author
        this.sort = sort
        this.isDraft = isDraft
    }

    constructor(
        book: Book,
    ) : super() {
        this.id = book.id
        this.bitmap = book.bitmap
        this.idCoverBook = book.idCoverBook
        this.name = book.name
        this.author = book.author
        this.sort = book.sort
        this.isDraft = book.isDraft
    }

    override fun toString(): String {
        return "SmallBook(id=$id, observeBitmap=$observeBitmap, idCoverBook=$idCoverBook, name='$name', author=$author, sort=$sort, isDraft=$isDraft)"
    }


}

open class ReadBook : SmallBook {
    var statusReadingBook: StatusReadingBook = StatusReadingBook.NOT_IN_LIBRARY
    var genre: Genres = Genres.VARIOUS
    var completeness: StatusCreatedBooks = StatusCreatedBooks.IN_PROCESS

    constructor(resources: Resources) : super(resources)
    constructor(bookLibrary: LibraryBook, resources: Resources) : super(resources) {
        this.name = bookLibrary.name.toString()
        this.isDraft = bookLibrary.isDraft
        if (bookLibrary.genreId in 1..9) {
            this.genre = Genres.getGenre(bookLibrary.genreId)
        }
        if (bookLibrary.completenessId in 1..2) {
            this.completeness = StatusCreatedBooks.getStatusBook(bookLibrary.completenessId)
        }
        statusReadingBook = if (bookLibrary.statusId in 1..5)
            StatusReadingBook.getStatusReadingBook(bookLibrary.statusId)
        else {
            StatusReadingBook.NOT_IN_LIBRARY
        }
        this.idCoverBook = bookLibrary.coverId
    }

    constructor(
        id: Int,
        bitmap: Bitmap,
        idCoverBook: Int,
        name: String,
        author: Person,
        sort: Long,
        isDraft: Boolean,
        statusReadingBook: StatusReadingBook,
        genre: Genres,
        completeness: StatusCreatedBooks,
    ) : super(id, bitmap, idCoverBook, name, author, sort, isDraft) {
        this.statusReadingBook = statusReadingBook
        this.genre = genre
        this.completeness = completeness
    }

    constructor(
        book: Book,
    ) : super(
        book.id,
        book.bitmap,
        book.idCoverBook,
        book.name,
        book.author,
        book.sort,
        book.isDraft
    ) {
        this.statusReadingBook = book.statusReadingBook
        this.genre = book.genre
        this.completeness = book.completeness
    }

    constructor(id: Int) : super(id) {}

    override fun toString(): String {
        return "ReadBook(statusReadingBook=$statusReadingBook, genre=$genre, completeness=$completeness)" + super.toString()
    }


}

open class FindBook :
    ReadBook {
    var annotate: String = ""
    var quantityOfViews: UInt = 0u

    constructor(searchBook: SearchBook, resources: Resources) : super(resources) {
        this.name = searchBook.name
        this.annotate = searchBook.annotation
        this.isDraft = searchBook.isDraft
        this.genre = Genres.getGenre(searchBook.genre)
        this.quantityOfViews = searchBook.views.toUInt()
        this.idCoverBook = searchBook.coverId
        this.isDraft = searchBook.isDraft
        this.sort = searchBook.sort
    }

    constructor(resources: Resources) : super(resources)
    constructor(
        id: Int,
        bitmap: Bitmap,
        idCoverBook: Int,
        name: String,
        author: Person,
        sort: Long,
        isDraft: Boolean,
        statusReadingBook: StatusReadingBook,
        genre: Genres,
        completeness: StatusCreatedBooks,
        annotate: String,
        quantityOfViews: UInt,
    ) : super(
        id,
        bitmap,
        idCoverBook,
        name,
        author,
        sort,
        isDraft,
        statusReadingBook,
        genre,
        completeness
    ) {
        this.annotate = annotate
        this.quantityOfViews = quantityOfViews
    }

    constructor(
        book: Book,
    ) : super(
        book.id,
        book.bitmap,
        book.idCoverBook,
        book.name,
        book.author,
        book.sort,
        book.isDraft,
        book.statusReadingBook,
        book.genre,
        book.completeness
    ) {
        this.annotate = book.annotate
        this.quantityOfViews = book.quantityOfViews
    }

    constructor(id: Int) : super(id) {}
    override fun toString(): String {
        return "FindBook(annotate='$annotate', quantityOfViews=$quantityOfViews)" + super.toString()
    }

}

class Book : FindBook {
    var date: String = ""
    var idLastChapter: Int = -1

    constructor(resources: Resources) : super(resources)
    constructor(bookClass: BookClass, resources: Resources) : super(resources) {
        this.isDraft = bookClass.isDraft
        this.name = bookClass.name
        this.annotate = bookClass.annotation
        this.genre = Genres.getGenre(bookClass.genreId)
        this.completeness = StatusCreatedBooks.getStatusBook(bookClass.completenessId)
        this.date = bookClass.date
        this.quantityOfViews = bookClass.views.toUInt()
    }

    constructor(
        id: Int,
        bitmap: Bitmap,
        idCoverBook: Int,
        name: String,
        author: Person,
        sort: Long,
        isDraft: Boolean,
        statusReadingBook: StatusReadingBook,
        genre: Genres,
        completeness: StatusCreatedBooks,
        annotate: String,
        quantityOfViews: UInt,
        date: String,
        idLastChapter: Int,
    ) : super(
        id,
        bitmap,
        idCoverBook,
        name,
        author,
        sort,
        isDraft,
        statusReadingBook,
        genre,
        completeness,
        annotate,
        quantityOfViews
    ) {
        this.date = date
        this.idLastChapter = idLastChapter
    }

    constructor(
        book: Book,
    ) : super(
        book.id,
        book.bitmap,
        book.idCoverBook,
        book.name,
        book.author,
        book.sort,
        book.isDraft,
        book.statusReadingBook,
        book.genre,
        book.completeness,
        book.annotate,
        book.quantityOfViews
    ) {
        this.date = book.date
        this.idLastChapter = book.idLastChapter
    }

    constructor(id: Int) : super(id) {}
    override fun toString(): String {
        return "Book(date='$date', idLastChapter=$idLastChapter)" + super.toString()
    }


}
