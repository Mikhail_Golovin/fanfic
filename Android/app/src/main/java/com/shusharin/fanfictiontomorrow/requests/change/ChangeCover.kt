package com.shusharin.fanfictiontomorrow.requests.change

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.RequestCover
import retrofit2.Call


class ChangeCoverParams(val idCover: Int, val bitmap: Bitmap) {
}

class ChangeCover(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : RequestCover<Boolean, ChangeCoverParams>(startRefresh, stopRefresh) {
    val liveIsChanged: MutableLiveData<Boolean>
        get() = liveData

    fun sendRequest(idCover: Int, bitmap: Bitmap) {
        sendRequest(ChangeCoverParams(idCover, bitmap))
    }

    override fun analysisOfResponseBody(responseBody: Boolean, params: ChangeCoverParams) {
        setLiveData(responseBody)
    }

    override fun call(params: ChangeCoverParams): Call<Boolean> {
        val dataMultiPart = convertToPart(params.bitmap)
        return service.changeCover(params.idCover, dataMultiPart)
    }
}