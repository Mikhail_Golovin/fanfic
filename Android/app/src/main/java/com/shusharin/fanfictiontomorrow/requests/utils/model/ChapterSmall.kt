package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter

class ChapterSmall() {
    @JsonProperty
    var name: String = ""

    @JsonProperty
    var text: String = ""

    constructor(name: String, text: String) : this() {
        this.name = name
        this.text = text
    }

    constructor(chapter: Chapter) : this() {
        this.name = chapter.name
        this.text = chapter.text
    }

    override fun toString(): String {
        try {
            return ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this)
        } catch (e: JsonProcessingException) {
            e.printStackTrace()
        }
        return ""
    }
}