package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonProperty

class ChaptersInBook {
    @JsonProperty
    var chapterId = 0

    @JsonProperty
    var name = ""

    @JsonProperty
    var numberInBook: Int = 0

    constructor() {}
    constructor(chapterId: Int, name: String, numberInBook: Int) {
        this.chapterId = chapterId
        this.name = name
        this.numberInBook = numberInBook
    }

    override fun toString(): String {
        return "ChaptersInBook(chapterId=$chapterId, name='$name')"
    }

}