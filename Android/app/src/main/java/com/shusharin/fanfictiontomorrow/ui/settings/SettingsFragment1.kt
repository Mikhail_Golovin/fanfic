package com.shusharin.fanfictiontomorrow.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.preference.PreferenceFragmentCompat
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment

class SettingsFragment1 : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preference_info, null)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        MyViewFragment.appBar(
            requireActivity(),
            requireContext(),
            parentFragmentManager,
            viewLifecycleOwner
        )
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}