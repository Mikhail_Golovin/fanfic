package com.shusharin.fanfictiontomorrow.requests.get_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetLastChapterTest : RequestTestBase() {
    private lateinit var mockRequest: GetLastChapter

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getLastChapter)
    }

    @Test
    fun sendRequestFake() {
        sendRequest(mockRequest, mockk(), 1)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val lastChapter = slotInt.captured
        assert(lastChapter == 1)
    }
}