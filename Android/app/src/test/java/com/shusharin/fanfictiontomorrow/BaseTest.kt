package com.shusharin.fanfictiontomorrow

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shusharin.fanfictiontomorrow.requests.*
import com.shusharin.fanfictiontomorrow.requests.utils.classes.*
import com.shusharin.fanfictiontomorrow.requests.utils.model.ChaptersInBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class BaseTest {
    protected val liveDataPerson = mockk<MutableLiveData<Person>>()
    protected open var isMockAll = true
    val chapterInBookDefaultResponse = ChaptersInBook(1, "название", 1)
    val personDefaultResponse = Person(1, "Имя", "Фамилия")
    val bookDefaultResponse = Book(
        1,
        mockk(),
        1,
        "Название",
        personDefaultResponse,
        1,
        true,
        StatusReadingBook.READING,
        Genres.FANTASY,
        StatusCreatedBooks.IN_PROCESS,
        "Аннотация",
        15u,
        "21.11.2001",
        3
    )
    val findBookDefaultResponse = FindBook(bookDefaultResponse)
    val readBookDefaultResponse = ReadBook(bookDefaultResponse)
    val smallBookDefaultResponse = SmallBook(bookDefaultResponse)
    val genresDefaultResponse = mutableListOf(
        Genre(0, true),
        Genre(1, false),
        Genre(2, true),
        Genre(3, false),
        Genre(4, true),
        Genre(5, false),
        Genre(6, false),
        Genre(7, false),
        Genre(8, true),
    )
    protected var mockkMyApiServices =
        spyk(MyApiServices(mockk(), mockk(), this::addActiveRequest, this::removeActiveRequest))
    protected var myApiServices = spyk(mockkMyApiServices)
    protected val slotPerson = slot<Person>()
    protected val slotBook = slot<Book>()
    protected val slotFindBook = slot<FindBook>()
    protected val slotReadBook = slot<ReadBook>()
    protected val slotSmallBook = slot<SmallBook>()
    protected val slotChapter = slot<Chapter>()
    protected val slotBitmap = slot<Bitmap>()
    protected val slotInt = slot<Int>()
    protected val slotIntGetAuthUserId = slot<Int>()
    protected val slotBoolean = slot<Boolean>()
    protected val slotBooleanAddBookAll = slot<Boolean>()
    protected val slotBooleanChangeBookAll = slot<Boolean>()
    protected val slotGenres = slot<MutableList<Genre>>()
    protected val slotHashMapBook = slot<HashMap<Int, Book>>()
    protected val slotHashMapFindBook = slot<HashMap<Int, FindBook>>()
    protected val slotHashMapReadBook = slot<HashMap<Int, ReadBook>>()
    protected val slotHashMapChapter = slot<HashMap<Int, Chapter>>()

    @BeforeEach
    internal open fun setUpAll() {
        quantityActiveRequests = 0
        setReturnNotNullBitmap()

        myApiServices
        mockkApiRequest(
            this.mockkMyApiServices.getBookClass(),
            slotBook,
            Book(bookDefaultResponse),
            "getBookClass"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getChapter(),
            slotChapter,
            Chapter(chapterInBookDefaultResponse),
            "getChapter"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getLibraryBook(),
            slotReadBook,
            ReadBook(bookDefaultResponse),
            "getLibraryBook"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getMainPageBook(),
            slotSmallBook,
            SmallBook(bookDefaultResponse),
            "getMainPageBook"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getSearchBook(),
            slotFindBook,
            FindBook(bookDefaultResponse),
            "getSearchBook"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getCover(),
            slotBitmap,
            mockk(relaxed = true),
            "getCover"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getUser(),
            slotPerson,
            personDefaultResponse,
            "getUser"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getLastChapter(),
            slotInt,
            1,
            "getLastChapter"
        )
        mockkApiRequest(
            this.mockkMyApiServices.setDraft(),
            slotBoolean,
            true,
            "setDraft"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getPresentBookInGenre(),
            slotGenres,
            genresDefaultResponse,
            "getPresentBookInGenre"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getAuthUserId(),
            slotIntGetAuthUserId,
            1,
            "getAuthUserId"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdWriteBooks(),
            slotHashMapBook,
            hashMapOf(Pair(bookDefaultResponse.id, Book(bookDefaultResponse))),
            "getListIdWriteBooks"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdSearchBooks(),
            slotHashMapFindBook,
            hashMapOf(Pair(bookDefaultResponse.id, FindBook(bookDefaultResponse))),
            "getListIdSearchBooks"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdSearchBooksInLibrary(),
            slotHashMapReadBook,
            hashMapOf(Pair(bookDefaultResponse.id, ReadBook(bookDefaultResponse))),
            "getListIdSearchBooksInLibrary"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdLastBooks(),
            slotHashMapFindBook,
            hashMapOf(Pair(bookDefaultResponse.id, FindBook(bookDefaultResponse))),
            "getListIdLastBooks"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdGenreBooks(),
            slotHashMapFindBook,
            hashMapOf(Pair(bookDefaultResponse.id, FindBook(bookDefaultResponse))),
            "getListIdGenreBooks"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdChapters(),
            slotHashMapChapter,
            hashMapOf(Pair(bookDefaultResponse.id, Chapter(chapterInBookDefaultResponse))),
            "getListIdChapters"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdBooksLibrary(),
            slotHashMapReadBook,
            hashMapOf(Pair(bookDefaultResponse.id, ReadBook(bookDefaultResponse))),
            "getListIdBooksLibrary"
        )
        mockkApiRequest(
            this.mockkMyApiServices.getListIdBooksInSection(),
            slotHashMapFindBook,
            hashMapOf(Pair(bookDefaultResponse.id, FindBook(bookDefaultResponse))),
            "getListIdBooksInSection"
        )
        mockkApiRequest(this.mockkMyApiServices.deleteChapter(), slotBoolean, true, "deleteChapter")
        mockkApiRequest(this.mockkMyApiServices.changeCover(), slotBoolean, true, "changeCover")
        mockkApiRequest(this.mockkMyApiServices.changeChapter(), slotBoolean, true, "changeChapter")
        mockkApiRequest(this.mockkMyApiServices.changeBook(), slotInt, 1, "changeBook")
        mockkApiRequest(this.mockkMyApiServices.addCover(), slotInt, 1, "addCover")
        mockkApiRequest(this.mockkMyApiServices.addBook(), slotInt, 1, "addBook")
        mockkApiRequest(
            this.mockkMyApiServices.addBookAll(),
            slotBooleanAddBookAll,
            true,
            "addBookAll"
        )
        mockkApiRequest(
            this.mockkMyApiServices.changeBookAll(),
            slotBooleanChangeBookAll,
            true,
            "changeBookAll"
        )
        mockkApiRequest(this.mockkMyApiServices.addBook(), slotInt, 1, "addBook")
        mockkApiRequest(
            this.mockkMyApiServices.addBookInLibrary(),
            slotBoolean,
            true,
            "addBookInLibrary"
        )
        mockkApiRequest(this.mockkMyApiServices.addChapter(), slotInt, 1, "addChapter")

        mockkStatic(Looper::class)
        val looperMock = mockk<Looper>()
        val threadMock = mockk<Thread>()
        every { looperMock.thread } returns threadMock
        every { Looper.getMainLooper() } returns looperMock

        mockkConstructor(Handler::class)
        every { anyConstructed<Handler>().postDelayed(any(), any()) } answers {
            val run = it.invocation.args[0] as Runnable
            run.run()
            true
        }

        every { anyConstructed<Handler>().post(any()) } answers {
            val run = it.invocation.args[0] as Runnable
            try {
                run.run()
            } catch (_: Exception) {

            }
            true
        }

//        mockkConstructor(MyRequest::class)
//        every { anyConstructed<MyRequest<*, *, Person>>().setLiveData(capture(slotPerson)) } answers {
//            println(slotPerson.captured)
//        }


    }

//    private fun mockkMyApiRequest(kFunction0: KFunction0<GetBookClass>) {
//        every { kFunction0() } answers {
//            {
//                isMockAll = false
//                callOriginal()
//                isMockAll = true
//            }
//        }
//    }

//    private inline fun <reified RequestType : Any, reified RequestParams, reified RequestLiveData : Any> mockkApiRequest(
//        request: MyRequest<RequestType, RequestParams, RequestLiveData>,
//        slot: CapturingSlot<RequestLiveData>,
//        classObserve: RequestLiveData,
//        string: String,
//    ) {
//        every { this@BaseTest.mockkMyApiServices[string]() } answers {
//            slot.clear()
//            val mockRequestBookClass = spyk(request, recordPrivateCalls = true)
//            mockkLiveData(mockRequestBookClass, slot, classObserve)
//            if (isMockAll) {
//                mockCall(mockk(relaxed = true), mockRequestBookClass, 3)
//            }
//            mockRequestBookClass
//        }
//    }

    private inline fun <reified RequestType : Any, reified RequestParams, reified RequestLiveData : Any, reified Request : MyRequest<RequestType, RequestParams, RequestLiveData>> mockkApiRequest(
        request: Request,
        slot: CapturingSlot<RequestLiveData>,
        classObserve: RequestLiveData,
        string: String,
    ) {
        every { this@BaseTest.mockkMyApiServices[string]() } answers {
            slot.clear()
            val mockRequestBookClass = spyk(request, recordPrivateCalls = true)
            mockkLiveData(mockRequestBookClass, slot, classObserve)
            if (isMockAll) {
                mockCall(mockk(relaxed = true), mockRequestBookClass, 3)
            }
            if (mockRequestBookClass is IGetSearchBook) {
                every {
                    mockRequestBookClass.getSearchBook(
                        any(),
                        any<ListBookId>(),
                        any()
                    )
                } answers {
                    val listBookId = args[1] as ListBookId
                    for (i in listBookId.booksId) {
                        mockRequestBookClass.setSearchBook(FindBook(i))
                    }
                }
            }
            if (mockRequestBookClass is IGetBookClass) {
                every {
                    mockRequestBookClass.getGetBookClass(
                        any(),
                        any()
                    )
                } answers {
                    val listBookId = args[1] as ListBookId
                    for (i in listBookId.booksId) {
                        mockRequestBookClass.setBookClass(Book(i))
                    }
                }
            }
            if (mockRequestBookClass is IGetLibraryBook) {
                every {
                    mockRequestBookClass.getLibraryBook(
                        any(),
                        any<ListBookId>(),
                    )
                } answers {
                    val listBookId = args[1] as ListBookId
                    for (i in listBookId.booksId) {
                        mockRequestBookClass.setLibraryBook(ReadBook(i))
                    }
                }
            }
            mockRequestBookClass
        }
    }

    inline fun <RequestLiveData, reified RequestParams, reified RequestType : Any> mockCall(
        responseBody: RequestType?,
        request: MyRequest<RequestType, RequestParams, RequestLiveData>,
        typeSend: Int = 1,
    ): Pair<Response<RequestType>, Call<RequestType>> {
        val mockedResponse = mockk<Response<RequestType>>(relaxed = true)

        every { mockedResponse.body() } returns responseBody

        val mockedCall = mockkCall(mockedResponse, typeSend)
        every {
            request.call(any())
        } returns mockedCall
        return Pair(mockedResponse, mockedCall)
    }

    inline fun <reified RequestType : Any> mockkCall(
        mockedResponse: Response<RequestType>,
        typeSend: Int = 1,
    ): Call<RequestType> {
        val mockedCall = mockk<Call<RequestType>>(relaxed = true)
        val mockedThrowable = mockk<Throwable>(relaxed = true)
        every { mockedThrowable.printStackTrace() } returns println("printStackTrace")
        every { mockedCall.clone() } returns mockedCall
        every { mockedCall.enqueue(any<Callback<RequestType>>()) } answers {
            val callback = args[0] as Callback<RequestType>

            when (typeSend) {
                1 -> callback.onResponse(mockedCall, mockedResponse)
                2 -> callback.onFailure(mockedCall, mockedThrowable)
                3 -> callback.toString()
            }
        }
        return mockedCall
    }

    inline fun <RequestType, RequestParams, reified RequestLiveData : Any> mockkLiveData(
        mockRequest: MyRequest<RequestType, RequestParams, RequestLiveData>,
        slot: CapturingSlot<RequestLiveData>,
        classObserve: RequestLiveData,
    ) {
        every { mockRequest["setLiveData"](capture(slot)) } answers {
            println(slot.captured)
        }
        mockObserve1(mockRequest, classObserve)
    }


    inline fun <RequestType, RequestParams, reified RequestLiveData : Any> mockObserve1(
        mockRequest: MyRequest<RequestType, RequestParams, RequestLiveData>,
        classObserve: RequestLiveData,
    ) {
        val liveData = mockk<MutableLiveData<RequestLiveData>>()
        every {
            liveData.observe(
                any(), any()
            )
        } answers {
            try {
                val run = it.invocation.args[1] as Observer<RequestLiveData>
                run.onChanged(classObserve)
            } catch (_: Exception) {
            }
        }
        every { mockRequest.liveData } returns liveData
    }

    protected fun setReturnNotNullBitmap() {
        mockkStatic(BitmapFactory::class)
        val bitmapMock = mockk<Bitmap>()
        every { BitmapFactory.decodeStream(any()) } returns bitmapMock
        every { BitmapFactory.decodeResource(any(), any()) } returns bitmapMock
    }

    protected fun verifyReturnNotNullBitmap() {
        verify { BitmapFactory.decodeStream(any()) }
    }

    private fun addActiveRequest() {
        quantityActiveRequests++
    }

    private fun removeActiveRequest() {
        quantityActiveRequests--
    }

    var quantityActiveRequests = 0
}