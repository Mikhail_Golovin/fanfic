package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test5: Base() {
    @Test
    fun `Checking the correctness of the issuance of books in the section Hot novelties`() {
        val bookInHot = getListIdBooksInSection(Sections.HOT_NEW_ITEMS)
        for (idBook in bookInHot) {
            val book = getBookClass(-1, idBook.key)
            val period = getAntiquity(book)
            Assertions.assertTrue(period.years == 0)
            Assertions.assertTrue(2 >= period.months)
        }

        for (genre in Genres.values()) {
            slotHashMapFindBook.clear()
            val bookInGenre = getListIdGenreBooks(genre.id)
            for (idBookResponse in bookInGenre) {
                if (!bookInHot.containsKey(idBookResponse.key)) {
                    val book = getBookClass(-1, idBookResponse.key)
                    val period = getAntiquity(book)
                    if (period.years == 0) {
                        Assertions.assertTrue(period.months > 2)
                    }
                }
            }
        }
    }
}