package com.shusharin.fanfictiontommorow.ui.end_to_end

import android.content.Intent
import android.view.View
import android.widget.TextView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.shusharin.fanfictiontommorow.ui.end_to_end.common.DoubleRecyclerViewMatcher
import com.shusharin.fanfictiontommorow.ui.end_to_end.common.RecyclerViewMatcher
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.login.LoginViewModel
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewModelFactory
import org.junit.Before
import org.junit.Rule
import java.lang.Thread.sleep


open class Base {
    protected lateinit var scenario: ActivityScenario<MainActivity>
    private val intent =
        Intent(ApplicationProvider.getApplicationContext(), MainActivity::class.java)

    @get:Rule
    val activityRule = ActivityScenarioRule<MainActivity>(intent)

    @Before
    open fun create() {
        scenario = ActivityScenario.launch(MainActivity::class.java)
    }

    protected fun login() {
        openInDrawerLayout(R.id.nav_login)
        //        Espresso.onView(ViewMatchers.withId(R.id.sign_in_button)).perform(ViewActions.click())
        scenario.onActivity {
            val viewModel = ViewModelProvider(
                MainActivity.viewModelStore,
                MyViewModelFactory(MainActivity.api)
            )[LoginViewModel::class.java]
            viewModel.getAuthUserId.sendRequest("Andrey", "Shusharin", "1a2n3drey@gmail.com")
        }
        sleep(5000)
    }

    protected fun openInDrawerLayout(id: Int) {
        scenario.onActivity { activity ->
            val drawerLayout = activity.findViewById<View>(R.id.drawer_layout) as DrawerLayout
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN)
        }

        Espresso.onView(ViewMatchers.withId(R.id.drawer_layout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        clickOnView(id)
    }

    protected fun logout() {
        sleep(5000)
        openInDrawerLayout(R.id.nav_logout)

        clickOnTextInViewInDialog(R.string.dialog_yes)
    }

    protected fun clickOnTextInViewInDialog(idString: Int) {
        clickOnTextInViewInDialog(getString(idString))
    }

    private fun getString(idString: Int): String {
        var textInView = ""
        scenario.onActivity { activity ->
            textInView = activity.getString(idString)
        }
        return textInView
    }

    protected fun clickOnTextInViewInDialog(textInView: String) {
        Espresso.onView(ViewMatchers.withText(textInView))
            .inRoot(RootMatchers.isDialog())
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }

    protected fun clickOnView(id: Int) {
        Espresso.onView(ViewMatchers.withId(id))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }

    protected fun clickOnViewStringResource(idString: Int) {
        clickOnView(getString(idString))
    }

    protected fun clickOnView(text: String) {
        Espresso.onView(ViewMatchers.withText(text))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }

    protected fun getInChapter(id: Int): String {
        var secondText1 = ""
        scenario.onActivity {
            secondText1 = it.findViewById<TextView>(id).text.toString()
        }
        return secondText1
    }

    protected fun getArrayResources(idArrayResources: Int, position: Int):String {
        var themeName =""
        scenario.onActivity {
            themeName = it.resources.getStringArray(idArrayResources)[position]
        }
        return themeName
    }

    fun openBookInPopulate() {
        val idSection = Sections.POPULATE.idSection
        Espresso.onView(ViewMatchers.withId(R.id.list)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                idSection - 1
            )
        )
        Espresso.onView(
            DoubleRecyclerViewMatcher(R.id.list).atPositionOnView(
                idSection - 1,
                R.id.listBooks,
                0,
                R.id.cornerCon
            )
        ).check(ViewAssertions.matches(ViewMatchers.isDisplayed())).perform(ViewActions.click())
    }

    protected fun setText(idEditText: Int, text: String) {
        Espresso.onView(ViewMatchers.withId(idEditText))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.replaceText(text))
    }

    private fun clickOnItemInRecycleView(recyclerViewMatcher: RecyclerViewMatcher, idItem: Int, position: Int) {
        Espresso.onView(
            recyclerViewMatcher.atPositionOnView(
                position,
                idItem,
            )
        ).perform(ViewActions.click())
    }
    protected fun clickOnItemInRecycleView(idRecycle: Int, idItem: Int, position: Int) {
        scrollTo(idRecycle, position)
        clickOnItemInRecycleView(RecyclerViewMatcher(idRecycle),idItem,position)
    }

    protected fun scrollTo(idRecycle: Int, position: Int) {
        Espresso.onView(ViewMatchers.withId(idRecycle)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                position
            )
        )
    }
}