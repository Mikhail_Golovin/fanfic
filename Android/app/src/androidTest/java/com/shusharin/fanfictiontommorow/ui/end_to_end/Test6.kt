package com.shusharin.fanfictiontommorow.ui.end_to_end

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontommorow.ui.end_to_end.common.RecyclerViewMatcher
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test6 : BaseAuth() {

    @Test
    fun openingChapterViaTableOfContents() {
        clickOnView(R.id.nav_my_profile)
        clickOnView(R.id.library)
        clickOnItemInRecycleView(R.id.list, -1, 0)
        clickOnView(R.id.table_of_contents)
        clickOnItemInRecycleView(R.id.list, -1, 0)
        val nameChapterOpen = MainActivity.toolbar.title
        assertEquals((RecyclerViewMatcher.getFindViewLast() as TextView).text, nameChapterOpen)
    }
}