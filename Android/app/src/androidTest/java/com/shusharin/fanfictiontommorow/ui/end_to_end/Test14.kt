package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import org.junit.Assert.assertNotEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test14 : BaseAuth() {
    @Test
    fun backFromTheSecondToTheFirstChapter() {
        clickOnView(R.id.nav_my_profile)
        clickOnView(R.id.library)
        clickOnItemInRecycleView(R.id.list, -1, 0)
        clickOnView(R.id.table_of_contents)
        clickOnItemInRecycleView(R.id.list, -1, 1)
        val nameChapterOpen1 = MainActivity.toolbar.title
        clickOnView(R.id.left)
        val nameChapterOpen2 = MainActivity.toolbar.title
        assertNotEquals(nameChapterOpen1, nameChapterOpen2)
    }
}