package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test17 :BaseAuth(){
    @Test
    fun openingProfileWhileReading(){
        clickOnView(R.id.nav_my_profile)
        clickOnView(R.id.library)
        clickOnItemInRecycleView(R.id.list, -1, 0)
        clickOnView(R.id.table_of_contents)
        clickOnItemInRecycleView(R.id.list, -1, 1)
        clickOnView(R.id.nav_my_profile)
        TestCase.assertEquals(
            R.id.nav_my_profile,
            MainActivity.navController.currentDestination!!.id
        )
    }
}