package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test11 : Base() {
    @Test
    fun switchingTheStartScreen() {
        openInDrawerLayout(R.id.nav_settings)
        clickOnViewStringResource(R.string.open_in_start)
        clickOnTextInViewInDialog(getArrayResources(R.array.open_window_entries, 1))
    }

    @Test
    fun switchingTheStartScreen2() {
        assertEquals(R.id.nav_my_library, MainActivity.navController.currentDestination!!.id)
    }

    @Test
    fun switchingTheStartScreen3() {
        openInDrawerLayout(R.id.nav_settings)
        clickOnViewStringResource(R.string.open_in_start)
        clickOnTextInViewInDialog(getArrayResources(R.array.open_window_entries, 0))
    }
}