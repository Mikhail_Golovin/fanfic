package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import org.junit.Test
import org.junit.runner.RunWith

private const val CAT = "кот"

@RunWith(AndroidJUnit4::class)
class Test13 : Base() {
    @Test
    fun workOfTheAuthorOfTheBook() {
        Thread.sleep(1000)
        clickOnView(R.id.app_bar_search)
        Thread.sleep(1000)
        setText(R.id.text_name_book, CAT)

        clickOnView(R.id.name_genre)
        clickOnTextInViewInDialog(Genres.FANTASY.nameG)
        clickOnTextInViewInDialog(R.string.dialog_ok)

        clickOnView(R.id.search)
        Thread.sleep(7000)
        clickOnItemInRecycleView(R.id.list, -1, 0)
        Thread.sleep(5000)
        clickOnView(R.id.nameAuthor)
        clickOnView(R.id.works)
    }
}