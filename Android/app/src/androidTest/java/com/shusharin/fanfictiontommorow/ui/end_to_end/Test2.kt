package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontommorow.ui.end_to_end.common.RecyclerViewMatcher
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.ui.utils.Sorting
import org.hamcrest.CoreMatchers.containsString
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

private const val LIVE = "Жизнь"

@RunWith(AndroidJUnit4::class)
class Test2 : Base() {

    @Test
    fun bookSearch() {
        sleep(1000)
        clickOnView(R.id.app_bar_search)
        sleep(1000)
        setText(R.id.text_name_book, LIVE)

        clickOnView(R.id.name_genre)
        clickOnTextInViewInDialog(Genres.DETECTIVE.nameG)
        clickOnTextInViewInDialog(R.string.dialog_ok)

        clickOnView(R.id.name_sorting)
        clickOnTextInViewInDialog(Sorting.BY_NOVELTY.nameSoring)
        clickOnTextInViewInDialog(R.string.dialog_ok)

        clickOnView(R.id.search)

        var length = 0
        scenario.onActivity {
            length = it.findViewById<RecyclerView>(R.id.list).adapter!!.itemCount
        }

        for (i in 0 until length) {
            onView(withId(R.id.list)).perform(
                RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                    i
                )
            )
            onView(
                RecyclerViewMatcher(R.id.list).atPositionOnView(
                    i,
                    R.id.nameBook,
                )
            ).check(matches(ViewMatchers.isDisplayed()))
                .check(matches(withText(containsString(LIVE))))

            println(i)
        }
    }
}