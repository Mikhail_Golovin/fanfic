package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontommorow.ui.end_to_end.common.DoubleRecyclerViewMatcher
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.get_queue.GetLibraryBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.book.BookViewModel
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewModelFactory
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test3:BaseAuth() {

    @Test
    fun addingBookFromRecentSectionToLibrary() {
        Espresso.onView(ViewMatchers.withId(R.id.list)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                Sections.LAST.idSection-1
            )
        )
        Espresso.onView(
            DoubleRecyclerViewMatcher(R.id.list).atPositionOnView(
                Sections.LAST.idSection-1,
                R.id.listBooks,
                0,
                R.id.cornerCon
            )
        ).check(ViewAssertions.matches(ViewMatchers.isDisplayed())).perform(ViewActions.click())

        clickOnView(R.id.lists)

        clickOnTextInViewInDialog(StatusReadingBook.NOT_INTERESTING.nameSRB)

        clickOnTextInViewInDialog(R.string.dialog_ok)

        var getLibraryBook : GetLibraryBook
        var isWaiting = true
        scenario.onActivity { activity ->
            val viewModel = ViewModelProvider(
                MainActivity.viewModelStore,
                MyViewModelFactory(MainActivity.api)
            )[BookViewModel::class.java]
            val idBook = viewModel.liveBook.value!!.id
            val api = MyApiServices(activity,activity.resources,{},{})
            getLibraryBook = api.getLibraryBook()
            getLibraryBook.liveLibraryBook.observe(activity) {
                TestCase.assertEquals(it.statusReadingBook.id, StatusReadingBook.NOT_INTERESTING.id)
                isWaiting = false
            }
            getLibraryBook.sendRequest(MainActivity.idUser, idBook)
        }
        while (isWaiting) {
            Thread.sleep(10)
        }

    }
}